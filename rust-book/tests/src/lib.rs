// By convention this is the default library crate and receives the same name as
// the package. Given that it's a library, it can have integration tests against
// its public API

pub fn add_two(n: usize) -> usize {
    n + 2
}
