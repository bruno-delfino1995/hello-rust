// Since these are "test crates" we don't need to create the module with
// conditional compilation by using `#[cfg(test)]` because Cargo won't compile
// this by default and will only look at this when we run `cargo test`. Which
// also gives the option to run only a particular test by filename when using
// `cargo test --test <filename>`.
// Note that integration tests are only available for library crates. Since
// binary crates are meant to be run on their own, they don't expose any API.
// That's why the convention is to have a straightforward `src/main.rs` which
// delegates to `src/lib.rs` for the "program logic", this way we can have
// integration tests for our functionality and test the small portion of the
// binary in `src/main.rs` by hand

// Each integration test inside `tests` is a crate by itself. That's why we need
// to import the library crate we're developing prior to checking its behavior.
use tests;

mod common;

#[test]
fn it_adds_two() {
    let n = common::setup();
    assert_eq!(n + 2, tests::add_two(n));
}

