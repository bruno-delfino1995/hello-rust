// This is an alternative way to declare a "single file" module, you just create
// the directory with the name you want and the `mod.rs` file inside of it is
// the root for that module, so it'll be the file loaded when some source loads
// it by using `mod common`. This is a technique to prevent files used for
// helpers from being considered integration tests as `cargo test` only runs
// integration tests at the root of `tests` and don't go to subdirectories

pub fn setup() -> usize {
    println!("I'm not a test. I'm a helper")
    8
}
