// Within Rust there're two different types of errors: (1) recoverable errors
// using the type Result<T, E>; (2) unrecoverable errors using the macro panic!.
// The first allows the programmer to pattern match and handle failures properly
// while the second is like a bomb explosion which doesn't allow the programmer
// to contain it.
// A good rule of thumb for when to choose panic over Result is to prevent that
// your code end in a bad state due to some broken assumption, guarantee,
// contract, or invariant. One example of such bad state is the access of out of
// index elements on vectors, the behaviour is unknown and might lead to
// security holes. However, if you expect to be in such bad state, like a
// connection not being open at some time, and you have a way to encode this in
// your type system, it's better to use Result or the appropriate type so the
// caller can know what happened and react to the failure by either fixing what
// caused it or reporting to the user. On the other hand, if you end up with
// something unexpected or that doesn't make sense, the best choice is to panic
// because the message and handling is somewhat more appropriate to the
// programmer given that you panic probably due to some programming problem

use std::error::Error;
use std::fs::File;
use std::io::{self, ErrorKind, Read};

// You can use the `?` within any function that returns `Result` or `Option` or
// `std::ops::Try`. However, this isn't the case for the default not annotated
// type of main which is `()`, for this case we have to annotate main with a
// proper `Result` which Rust hopefully allows us to do so. For this case we are
// saying that main will return `()` in case of success and in case of failure a
// `Box<dyn Error>`, which means that it may return a memory container with an
// type that implements the `Error` trait, thus using a trait object to allow
// for values of different types
fn main() -> Result<(), Box<dyn Error>> {
    // Unrecoverable errors are mostly related to bugs that bring the
    // application to a halt. On its error message, the application will report
    // the line that caused the panic, independently where it is be it in your
    // code or someone else's
    // ```
    // panic!("it's fine");
    // ```

    // However, note that the message shows the line that caused the panic,
    // which means that it can be on some code that you don't even control. The
    // following code reports an error from the source code of Rust stdlib that
    // caused the panic. As this is an undefined behaviour and source security
    // vulnerabilities like buffer overread, Rust protects us from them by
    // panicking when reading instead of just reading whatever is in that memory
    // position like what C does
    // ```
    // let v = vec![1, 2, 3];
    // v[100];
    // ```

    // To see the tracing information to narrow the place that originated the
    // error you must set the environment variable `RUST_BACKTRACE` to `1`, so
    // Rust will know that you want to see stack traces.
    // RUST_BACKTRACE=1 cargo run
    // Just remember that for this you need to build your artifacts with
    // debugging information, which isn't included when building or running with
    // the `--release` flag

    // Instead of just halting at any problem, we can use the `Result` enum to
    // imbue the return type with the idea of possible crashes. By using Result
    // instead of panic we can show a proper message to the user or even
    // overcome the failure. Also, as it's an enum and Rust match operations
    // need to be exaustive, we get warnings for free from the compiler
    let file = match File::open(".hist") {
        Ok(f) => f,
        // Differently from `Option` the "wrong" side carries error information
        // on `Result`, so we can match for whatever might be inside and
        // workaround the error or just panic! in case there's nothing to do
        Err(e) => match e.kind() {
            ErrorKind::NotFound => match File::create(".hist") {
                Ok(f) => f,
                Err(e) => panic!("Problem creating file: {:?}", e),
            },
            e => panic!("Problem opening file: {:?}", e)
        },
    };

    // Match operators can be nested as many times as we need, however, the
    // outcome isn't pleasant. One alternative to that is the `unwrap_or_else`
    // which is an idiom for the latter code where you only want to deal with the
    // error case. Since it's a function and we need to provide a handler for
    // the else case, this function accepts an anonymous function that captures
    // its environment (a closure)
    let file = File::open(".hist").unwrap_or_else(|err| {
        if err.kind() == ErrorKind::NotFound {
            File::create(".hist").unwrap_or_else(|err| {
                panic!("Problem creating file: {:?}", err);
            })
        } else {
            panic!("Problem opening file: {:?}", err);
        }
    });

    // Another alternative is to just let everything panic if the Result isn't
    // an `Ok`, and for that there's `unwrap` and `expect`, with the latter
    // allowing us to customize the message used on `panic!`
    let file = File::open(".hist").unwrap();

    let file = File::open(".hist")
        .expect("The file .hist doesn't exist at the current directory");

    // If we haven't annotated main with the correct type annotation this
    // wouldn't be possible
    let user = read_username_from_file()?;

    Ok(())
}

// By using the Result enum as return type we gain a nothing that this function
// might fail and in such case it will return `Err(io::Error)`, otherwise it'll
// return `Ok(String)`
fn read_username_from_file() -> Result<String, io::Error> {
    let mut file = match File::open("user.txt") {
        Ok(f) => f,
        // One way to handle errors is to just don't. Sometimes the place that
        // generates the error doesn't have enough information or even doesn't
        // have the responsibility for handling the generated error. For these
        // cases we just need to wrap the error returned by the source into the
        // correct type, be it through conversions or just changing the wrap so
        // the type system doesn't get lost by the misalignment of the return of
        // `File::open` and the return of this function
        Err(e) => return Err(e),
    };

    let mut name = String::new();

    // As match is an expression and if we don't end with `;` it remains like
    // that, and by default a function returns the result of the last
    // expression; here we are returning the branches accordingly
    match file.read_to_string(&mut name) {
        Ok(_) => Ok(name),
        Err(e) => Err(e),
    }
}

// As reading from a file into a string is a common need, there's
// `fs::read_to_string` on the standard library
fn read_file(path: &str) -> Result<String, io::Error> {
    // The pattern of error propagation is so common within Rust that we have
    // the `?` operator to simplify the matches over Err and Ok. By using `?` we
    // are automatically unwrapping the value from the `Ok` container in case of
    // success, while on the case of `Err` it will get returned early
    let mut file = File::open(path)?;

    let mut contents = String::new();

    // The main difference from the match expression is that errors are
    // converted from the source type to the target type by using methods of the
    // `From` trait. These methods are defined on the target type by
    // implementing such trait with the source type as its type parameter, but
    // as we are using the same type we don't need to worry about implementing
    // the trait because there's no conversion
    file.read_to_string(&mut contents)?;

    let result: Result<String, io::Error> = Ok(contents);

    // Just because I've separated them doesn't mean that `?` can't be chained.
    let mut contents = String::new();

    File::open(path)?.read_to_string(&mut contents)?;

    Ok(contents)
}
