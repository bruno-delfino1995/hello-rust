// This brings everything within `std::io` into scope beneath the `io::name`. By
// default Rust only brings a handful of packages and call them `Prelude`, much
// like Haskell
use std::io;

// Brings Rng trait into scope, it defines methods used by random number
// generators and to use those the trait must be in scope
use rand::Rng;

// Ordering is an enum containing the possible comparision results between two
// comparable elements. You might ask why didn't we brought all the Trait
// containing the comparison methods, that's due to comparison being a common
// need on applications that all those Traits are included by default on the
// prelude - https://doc.rust-lang.org/std/prelude/index.html
use std::cmp::Ordering;

// Pretty standard function definition without parameter or return
fn main() {
  // It looks like a function, you call it like a function, but it's not a
  // function. It's a macro used for printing to standard output with a newline.
  // You might be wondering, how do we differentiate macros from functions? Just
  // watch for a `!` before the parameters list
  println!("Guess the number!");

  // Use a RNG local to the current thread of execution and seeded by the
  // operating system, from that generate a number on a range lower bound
  // inclusive but upper bound exclusive using `gen_range`, which is defined by
  // the trait `Rng` brought into scope using `use rand::Rng`. All of which can
  // be discovered by running `cargo doc --open` and searching for `rand`
  // documentation locally
  let secret_number = rand::thread_rng().gen_range(1, 101);

  // println!("Your secret number is: {}", secret_number);

  // Say loop and Rust will go brr brr brr until you die, because this boy ain't
  // gonna stop.
  // This creates an infinite loop that only stops when you use `break` or some
  // other form of excution skips/handoffs like `return`
  loop {
    println!("Please input your guess");

    // Creates a mutable heap allocated string. Whaaaaat??
    // Let's strip down this. We have three main parts, variable declaration,
    // making it mutable, and creating a new string.
    // - `let` is used to declare new variables or shadow previously declared
    // - `mut` allow mutation for this variable, thus we can rebind it to another
    // value of change the value altogether internally, like changing a property
    // of an object. Yes, Rust is this awesome and have a through and throughout
    // control over mutability
    // - `String::new` is where we instantiate a new heap allocated string. What
    // does that even mean? Do you remember your old C pointers that could be used
    // along with malloc to allow growing and shrinking some array? That roughly
    // summarises what heap allocated mean. Differently from the stack which is
    // managed by the CPU and is tightly coupled with function scopes and function
    // execution (variables within a function live only while the function lives),
    // the heap is a free space managed by the programmer or VM or a fucking good
    // compiler like Rust (which is kind of the programmer but in an
    // autonomous/generated way). When thinking about heap vs stack try
    // remembering of malloc in C.
    // > https://gribblelab.org/CBootCamp/7_Memory_Stack_vs_Heap.html#org4a16795
    let mut guess = String::new();
    // Beware that `::new` is somewhat like static methods on Java, but it's called
    // associated function because it's associated with the **type** `String` and
    // not with **instances** of `String`. And the last part `new()` is just a
    // convention for creating instances for a type.

    // Returns an instance of `std::io::Stdin`, why not using `new()` like
    // `String`? Don't ask me, go for Rust Core Developers. Here is where the use
    // statement from the beginning helps us, it allows us to write only
    // `io::stdin` instead of the full qualified name of `std::io::stdin`
    io::stdin()
      // `read_line` reads from stdin and puts into a String. That's why we need
      // to pass it a reference, denoted by `&`, along with the `mut` modifier.
      // The first allows us to share this piece of data/memory with other
      // functions without copying it all over the place, ressembling C pointers
      // and Java references. While the latter allows read_line to modify the
      // contents of `guess`, this way it can pipe from stdin directly to you
      // variable/memory space
      .read_line(&mut guess)
      // If the world was a perfect place where'd be the joy? Here we handle all
      // the uncertanty that the world presents to us using the `Result` type
      // returned by `read_line`. However, instead of dealing with `Ok` and `Err`
      // variants of the `Result` enum, we just let the program panics in case of
      // errors along with this message and the message whithin the error itself
      .expect("Failed to read line");

    // Here is an example of the shadowing properties of `let`, it allowed us to
    // use the same variable name from before (`guess`) without warning us. Also,
    // it's smart enough to allow us use the old `String` (which was infered by
    // the Rust fucking smart compiler) to trim and parse into a `u32` (this time
    // declared by us - unsigned 32 bit integer) by using the `str::parse`
    // function which somehow gets its target type infered from our declaration.
    // ~~Damn it Rust; you're a fucking smart boy!!!~~
    // NOTE: independently whether the user's input is a valid number or not, we
    // still need to trim it because `read_line` includes the line feed the user
    // pressed at the end to conclude its entry
    let guess: u32 = match guess.trim().parse() {
      Ok(num) => num,
      Err(_) => continue
    };
    // In this match you can see two nice features, one being that match is an
    // expressions and returns whatever the matched block returns - or bails out
    // if told so, e.g., `continue` for `Err` match; and the other is the
    // pattern syntax which was used to extract `num` from within `Ok`, by using
    // a named variable match/pattern, and to ignore the component inside `Err`,
    // by using `_` catchall value.
    // > https://doc.rust-lang.org/book/ch18-03-pattern-syntax.html


    // String interpolation? Pffft, good and old printf. By using placeholders we
    // can specify places where println needs to print variable values. This
    // format is what `format!` uses and has many variants like {:x} for types
    // that implement Debug, named parameters with {name}, ..., take a
    // look at `std::fmt` for more.
    println!("You guessed: {}", guess);

    // Here's another great use of references, why would you want to create a new
    // stack - or even worse, a heap element (shame on pass as reference
    // languages) - when all you want is to comparate it with something. Also,
    // note that once we only want to inspect its value, there's no need to
    // declare it a mutable reference because cmp doesn't want to change it,
    // otherwise it'd be on its declaration.
    // NOTE: Comparison is taken from the "method owner" perspective, e.g., if
    // guess is 50 but secret_number is 38 it'll return Ordering::Greater because
    // from the "owner" perspective he is greater than 38
    let order = guess.cmp(&secret_number);

    // This is a match expression made of arms. Arms consist of patterns on the
    // left side that will match (duh?) with the value passed on the beginning,
    // and on the right side lies the code that will run in case the match comes
    // true. Also, patterns should cover all the enum variants or at least provide
    // the `_` catch all pattern.
    // NOTE: match expressions run matches from top to bottom and stop on the
    // first match
    match order {
      Ordering::Less => println!("Too small!"),
      Ordering::Greater => println!("Too big!"),
      Ordering::Equal => {
        println!("You win!");
        break;
      }
    }
  }
}
