use std::fmt::Display;

use crate::Point;

pub struct NewsArticle {
    pub headline: String,
    pub location: String,
    pub author: String,
    pub content: String,
}

pub struct Tweet {
    pub username: String,
    pub content: String,
    pub reply: bool,
    pub retweet: bool,
}

// A trait defines a set of behaviours used to accomplish some purpose, in this
// instance we are saying that something can be considered a Summary if it
// implements the method signatures inside the trait's body, and once
// implemented by a target type will turn the type into a "Summarizable".
// A trait is declared using the keyword `trait` followed by the trait name with
// maybe generic type arguments and preceded by visibility modifiers. The name
// is used when declaring implementations and the visibility modifier will let
// someone import your trait from an external module and implement it for its
// own types. Also, if you don't let the trait as public, there'll be no way
// that a client of an internal type could call a trait's method since it needs
// to bring the trait into scope before calling a method from its contract, thus
// leaving us with a trait that is only used internally within our modules
pub trait Summary {
    // To require some method to be implemented we just put a semicolon instead
    // of providing a body for it
    fn summarize_author(&self) -> String;

    // In case you have a default behaviour for a function, you can implement it
    // and let the implementors choose whether or not to implement the function
    fn summarize(&self) -> String {
        // Also, as the trait needs to be implemented as a whole by the types,
        // in terms of functions without implementations, you can call any
        // function that is defined within this trait without a problem
        format!("(Read more from {}...)", self.summarize_author())
    }
}

// To adhere to a trait type, we must implement the target trait for a target
// type using `impl <Trait> for <Type>`. This let's Rust know that this type has
// an implementation for the given trait so it can be used where that trait is
// expected.
// When a trait have default implementations for all of its methods, you don't
// need to provide a body for those if you don't want to override such methods.
// All you have to do is just say to Rust that you want to "sign" the contract
// of a trait and give an empty body `impl <Trait> for <Type> {}`. However,
// since there's the summarize_author function we need to provide the
// implementation for it at least
impl Summary for NewsArticle {
    fn summarize_author(&self) -> String {
        format!("{}", self.author.chars().take(10).collect::<String>())
    }

    // To allow traits to evolve with their default implementations without
    // breaking the implementations, there's no difference in syntax whether you
    // are implementing a function without an implementation or you are
    // overriding the default implementation of a function in the trait
    fn summarize(&self) -> String {
        format!("{}, by {} ({})", self.headline, self.author, self.location)
    }
}

// Since a trait is a contract, somewhat ressembling interfaces from Java, any
// type can implement it, the implementation is not entangled to a specific
// type. Although, there's a rule that prevents people from implementing a trait
// for the same type in two separated crates, which would render Rust powerless
// once there's no way for it to know which one it should use. The rule states
// that you can only implement a trait on a type if either the trait or the type
// is local to your crate, so you can implement external traits to an internal
// type and internal traits to external types, but you can't implement external
// traits on external types, e.g.:
// - Display for Tweet: OK
// - Summary for Vec<T>: OK
// - Summary for Tweet or NewsArticle: OK
// - Display for Vec<T>: Not OK
// - rand::Rng for String: Not OK
impl Summary for Tweet {
    // This is the code which the default implementation for `summarize` will
    // call at runtime
    fn summarize_author(&self) -> String {
        format!("@{}", self.username)
    }
}

pub fn main() {
    let tweet = new_tweet();
    let news = new_article();

    // Once the trait we want to use is already in our scope, we can use its
    // methods on types that implement such trait, but before everything we need
    // to let Rust "import" the knowledge about such a trait. That is why we
    // needed to import the `rand::Rng` before using some methods from the
    // `rand` trait on the `guessing_game` exercise.
    println!("A news about: {}", news.summarize());

    // This will call the default implementation for summarize because Tweet
    // didn't overrid that
    println!("1 new tweet: {}", tweet.summarize());
}

// A trait system without a way to use the contract as argument has no use for
// complex software. The following syntax of declaring the type as `impl
// <Trait>` tells Rust that we allow any type which has an implementation for
// the trait `Summary`, so instead of wait for a concrete type, we're a little
// more open and expect an implementation for that contract/trait
fn notify(item: &impl Summary) {
    println!("Breaking news: {}", item.summarize());
}

// The `impl Trait` is syntax sugar for the __Trait Bound Syntax__ shown below.
// For just one argument that flexes over a trait it might be convenient, but
// when you have two types, it might be annoying to declare the same boundary
// for a type over and over, e.g.:
// `fn cross_post(orig: &impl Summary, cop: &impl Summary)` over
// `fn cross_post<T: Summary>(orig: &T, cop: &T)`
fn from_whom<T: Summary>(item: &T) {
    println!("{} published", item.summarize_author());
}

// In case you want an argument to implement more than one trait, you specify
// multiple trait bounds by "adding" the contracts with the following syntaxes:
// fn publish<T: Summary + Display>(item: &T)
fn publish(item: &(impl Summary + Display)) {
    println!("{} presents you {}", item.summarize_author(), item);
}

// By prepending the bounds before the argument list, it may get hard to read
// some functions that are generic with a bunch of conditions. For this there's
// the `where` clause on function definitions and any other place where your can
// put type parameters (enums, traits, structs, impl), that go as you can see
// below.
fn cross_post<T, U>(post: T, target: U)
where
    // It's the same thing that you'd put on the type parameters list, but
    // separated to keep the function's head more lean
    T: Summary + Clone,
    U: Display,
{
    let new = post.clone();
    println!("Post new {} at {}", post.summarize(), target);
}

// We can use implementations as return types as well. However, due to how `impl
// Trait` is implemented inside the compiler, we cannot return different types
// that share an implementation, we can only return a single type
fn new_tweet() -> impl Summary {
    Tweet {
        username: String::from("horse_ebooks"),
        content: String::from("of course, as you probably already know, people"),
        reply: false,
        retweet: false,
    }
}

fn new_article() -> impl Summary {
    NewsArticle {
        headline: String::from("Rust is awesome"),
        location: String::from("rust-lang.org"),
        content: String::from("Resistance is futille, join the horde"),
        author: String::from("Mozilla"),
    }
}

// The following function is invalid because we are using the `impl Trait`
// syntax to return two different types, also remember that `if` branches need
// to share a common type, so it can't also be just an `if ... else`. It seems
// that this can be implemented by __using trait objects that allow for values
// of different types__ that I don't know how yet
// ```
// fn new_summarizable(tweet: bool) -> impl Summary {
//     if tweet {
//         return new_tweet()
//     };
//
//     new_article()
// }
// ```

// We can have implementations specific for type parameters that comply to the
// trait bound, in this case only when the T have both PartialOrd and Display
// we'll have a `biggest` method available.
impl<T: Display + PartialOrd> Point<T> {
    fn biggest(&self) {
        match (self.x == self.y, self.x > self.y) {
            (true, _) => println!("? - x:{} y:{}", self.x, self.y),
            (_, true) => println!("X - x:{} y:{}", self.x, self.y),
            _ => println!("Y - x:{} y:{}", self.x, self.y)
        }
    }
}

// Another option is to use a trait bound to implement a trait for any type that
// implement another trait. These are called _blanket implementations_ and are
// useful for when you can derive a functionality from another trait, much like
// how the trait `ToString` can be implemented automatically for any T that
// implements `Display` as the followind implementation head shows (this is at
// the std according to the book) - `impl<T: Display> ToString for T { ... }` -
// thus allowing us to call `3.to_string()` because `i32` implements `Display`.
// The following will automatically implement the `Summary` trait for any `T`
// that implements `Display`, it's quite wrong when you consider that should be
// there an author for `summarize_author` but here I trancribe what I meant by
// deriving the functionality from another trait
impl<T: Display> Summary for T {
    fn summarize_author(&self) -> String {
        self.to_string()
    }
}
