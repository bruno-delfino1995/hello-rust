// Generics is one of the many tools to reduce duplication of concepts, they
// allow you to design your code against an abstract type that which is replaced
// by a concrete type when such code is called. This replacment is done by Rust
// while compiling the code in a process called monomorphization, which is the
// expansion of generic code such as Option<T> into concrete type as Option_i32
// or Option_Point thus removing the overhead for runtime and as consequence
// reducing the impact of generic code on performance

mod aggregator;
mod lifetimes;

// We can use generics when defining structs the same way we can do with
// functions and use it on the fields we want
#[derive(Debug)]
struct Point<T> {
    x: T,
    // Since we are using the same generic type, there's no way to create a
    // Point where x is one type and y is another
    y: T,
}

// The same syntax goes for enums, and since enums declare variations, we can
// have variations that use and others that don't. The only thing that we have
// to pay attention is that when the variant doesn't use the generic type, we
// need to declare it explicitly because Rust can't infer the type if the
// variant doesn't use the generic
enum Maybe<T> {
    Just(T),
    Nothing
}

// Multiple type parameters separated with commas.
enum Either<T, E> {
    // Since the variant only uses one of the type parameters, we need to
    // explicitly tell Rust the other, be it through the return type or variable
    // type
    Right(T),
    Left(E)
}

// On implementations we have two choices, create generic implementations over
// the type parameter by telling that it's generic over it. Once declared that
// the implementation is generic, we must use the same type parameter for the
// implementation target (Point<T> and not Point<R>) and then we'll be able to
// use it for associated or methods
impl<T> Point<T>
// The parameter belongs to the implementation, so any need that the methods or
// associated functions need, have to be written here instead of where the trait
// is used
where T: Copy
{
    fn x(&self) -> &T {
        &self.x
    }

    // Since the function doesn't declare type parameters and who does so is the
    // type, the turbofish operator will go on the type instead of the function.
    // The same applies to methods, however, some functions might need it such
    // as collect enable you to use the turbofish because it's declared as
    // `fn collect<B>(self) -> B` while others such as `fn into(self) -> T`
    // don't because they don't declare the need for a type parameter
    fn same(val: T) -> Point<T> {
        Point { x: val, y: val }
    }
}

// The other choice is to have implementations for concrete types, so only
// Point<i32> will be able to access these methods and associated functions
impl Point<f32> {
    fn distance(&self, another: &Self) -> f32 {
        let length = self.x - another.x;
        let height = self.y - another.y;

        (length.powi(2) - height.powi(2)).sqrt()
    }

    fn rectangle_from_origin(height: f32, length: f32) -> (Self, Self) {
        (Point { x: 0.0, y: 0.0 }, Point { x: length, y: height })
    }
}

struct Coord<T, U> {
    x: T,
    y: U,
}

impl<T, U> Coord<T, U> {
    // We can mixup different type parameters from different structures to
    // create a new type, which can even give a glimpse of what's being done by
    // the function to the client. By looking at the declaration, an attentive
    // programmer might guess just by looking at the signature that we'll use x
    // from the first because T is used on the first and on x while y from the
    // second because the second type for y is W.
    fn mixup<V, W>(self, other: Coord<V, W>) -> Coord<T, W> {
        Coord {
            x: self.x,
            y: other.y
        }
    }
}

fn main() {
    let numbers = vec![174, 19, 234, 189, -304];
    println!("The largest number of {:?} is {}", numbers, largest(&numbers));

    let chars = vec!['y', 'm', 'a', 'q'];
    println!("The greatest char in {:?} is {}", chars, largest(&chars));

    let numbers = vec![174, 19, 234, 189, -304];
    // This is a little bit special because it'll make the type of numbers be
    // Vec<i64> instead of the default Vec<i32>, that happens because it used the
    // type T of the return to define the T used by the parameter and made numbers
    // be that to match with the expected result
    let big: i64 = largest(&numbers);

    // Here the concrete type of Point will be Point<i32> because it infered by
    // the default integer type
    let origin = Point { x: 0, y: 0 };

    // Point<f64> as the default type of floats is f64
    let northeast = Point { x: 1.0, y: 1.0 };

    // Point<f32> because we explicitly set x to f32 so y will follow
    let southeast = Point { x: 1.0f32, y: -1.0 };

    // Declaring the type when constructing so the result is Point<i8>
    let southwest = Point::<i8> { x: -1, y: -1 };

    // Declaring the variable type and such the construction will follow
    let nortwest: Point<i16> = Point { x: -1, y: 1 };

    // We can use the turbofish (::<...>) notation for enum variants as well,
    // but it's not so readable as declaring the type variable
    let none = Maybe::Nothing::<i32>;
    let none: Maybe<i32> = Maybe::Nothing;

    // If we didn't declare the types we wouldn't be able to instantiate the
    // variant because there's no way for Rust to determine the `E` type
    // parameter
    let right = Either::<i32, i8>::Right(8);
    let right: Either<i32, i8> = Either::Right(8);

    // We can use "type holes" to prevent repetition
    let left = Either::<i8, _>::Left(8i16);
    let left: Either<i8, _> = Either::Left(8i16);

    // As the type parameter for the associated function lives under the
    // implementation and not under the function, we have to specify it before
    // the function
    let northeast = Point::<i8>::same(1);
    // Or let the Rust infer it by the parameter or return type
    let southwest = Point::same(6);
    let square: Point<f64> = Point::same(0.0);

    let origin: Point<f32> = Point::same(0.0);
    let northeast = Point::same(0.0);
    // Despite being created by an associated method from the generic
    // implementation, it's still an Point<f32> so we'll get the methods from
    // its implementation
    let distance = origin.distance(&northeast);
    println!("The distance from {:?} to {:?} is {}", origin, northeast, distance);

    // As associated functions live on a specific type, and not necessarily the
    // return type will give enough information for Rust to infer the type, we
    // might need to use the turbofish operator. In this case we need to specify
    // the elements of the tuple to have a link for inference, but if it were
    // something more complex, it might not worth specifying the whole type
    // while you can use the turbofish
    let rectangle_corners = Point::<f32>::rectangle_from_origin(1.0, 1.0);
    // As both elements from the tuple are the same, we can ignore the second
    let (bottom_left, top_right): (Point<f32>, _) = Point::rectangle_from_origin(1.0, 1.0);

    println!("\nLearn a little about traits\n");
    aggregator::main();

    println!("\nIsn't it a beautiful time to live?\n");
    lifetimes::main();
}

// To be able to use generics in a function we have to state in its name that
// it'll behave accordingly to some type parameters, which are then specified
// after the name but before the parameters enclosed with `<...>`. The following
// function would be read as __the function largest is generic over type T__.
// Once declared in the function that it's generic over some type or types (<T,
// R, U, ... Z>) we can use the generic types on parameters and return types.
// Enabling us to use it at same time for the array elements and for the return,
// and with Rust powerfull type inference, sometimes we don't need to specify
// the type parameter because it'll infer by the passed parameters, thus the
// following snippets are the same
// ```
// let numbers = vec![174, 19, 234, 189, -304];
// let big = largest::<i32>(numbers)
// ```
// let numbers: Vec<i32> = vec![174, 19, 234, 189, -304];
// let big = largest(&numbers)
// ```
// let numbers = vec![174, 19, 234, 189, -304];
// let big: i32 = largest(&numbers)
// ```
fn largest<T>(input: &[T]) -> T
// This trait bound will let Rust enforce the restrictions that this method have
// for the parametric type so that it can function correctly
where T: PartialOrd + Copy
{
    let mut result = input[0];

    for &el in input {
        if el > result {
            result = el;
        }
    }

    result
}
