// Lifetimes control the scope in which a reference is valid, they're what
// prevent us from using dangling pointers. Which are dangling by default
// because Rust clean everything when the scope ends, but they wouldn't be if we
// let them live more, and what lets them outlive their scope are lifetime
// annotations. Rust infers lifetimes by defaults, alike what it does with
// types, but when we want a tuned lifetime to indicate relations between
// different references, we must annotate them.

pub fn main() {
    // Lifetimes are what prevents us from creating a dangling reference on the
    // following example:
    // ```
    // // This variable seems to contradict the non-null restriction of Rust,
    // // but that is guaranteed once Rust disallows the use of it before it has
    // // a value
    // let endure;
    // {
    //     let fade = 8;
    //     endure = &fade;
    // }
    // println!("endured: {}", endure);
    // ```
    // The problem lies on the fact that `fade` doesn't live enough to be used
    // later through `endure` because it's bonded to the inner scope that ends
    // before `endure` is used. However, cases like these are wanted because we
    // want to save memory and return a reference to something that was
    // previously passed to us, if it weren't due to lifetimes it wouldn't be
    // possible

    let large = 2;

    {
        // The case of returning a reference to something that was passed is
        // demonstrated by the `largest` function, a function which choose to
        // return a reference instead of creating new data and wasting precious
        // memory. Although, don't think that you if you want to return
        // references all you have to do is use a lifetime, remember that
        // lifetimes are about the relation between references so Rust can know
        // when to end it. A lifetime can only be used if it's related to some
        // other reference, it can be brought to life on its own
        let small = 1;
        let largest = largest(&large, &small);
        println!("The largest number is: {}", largest);
        let greatest = greatest(&small, &large);
        println!("The greatest number is: {}", greatest);
    }
    // Given that the lifetime of the largest function result is related to all
    // the parameters, we wouldn't be able to use its result outside of that
    // inner scope because `small` would have been dropped by now. Remember that
    // when the same lifetime is related to more than one parameter, the
    // returned reference will live as long as the shortest lifetime.
    // We wouldn't have such problem using a function like
    // `fn first<'a>(x: &'a i32, y: &i32) -> &'a i32` because the lifetime
    // annotation connects the first parameter only, so since `large` outlives
    // `small` we will have no problem.

    let mut article = Article {
        author: &String::from("rust"),
        title: &String::from("lifetimes"),
        contents: &String::from("lifetimes are interesting")
    };
    {
        let contents = String::from("lifetimes are interesting");
        article = Article {
            contents: &contents,
            ..article
        };
        println!("Latest article: {:?}", article);
    }
    // Since the lifetime of the `Article` struct is bound to the lifetime of
    // `contents`, the following line would be invalid because `contents`
    // doesn't live enough to see the light out of its inner scope
    // println!("Latest article: {:?}", article)

    // Static lifetimes are special lifetimes that indicate that something can
    // live for the entire duration of the program. By default, since they are
    // stored directly in the program's binary, string literals have 'static
    // lifetimes.
    let immortal: &'static str = "I will outlive everything!!";
}

// The lifetime annotation is much like the generics annotation, we must tell
// Rust that we're going to use them by using the `<...>` before the function
// parenthesis. Although, they are a little different from generics in its
// notations, instead of upper case letters, we use a lower case letter
// prepended by an apostrophe. Then that lifetime annotation can be used on
// functions accordingly to the following examples:
// - &i32 // simple reference
// - &'a i32 // reference with lifetime 'a
// - &'a mut i32 // mutable reference with lifetime 'a
// On the arguments list is where we start to establish the relation between the
// parameter given and the return type, if both share the same lifetime
// annotation, they will have intertwined lifetimes where the returned
// references dies as soon the parent dies however the parent can outlive the
// reference.
// In this function we are relation all the references with the `'a` lifetime,
// that means that as soons as either `x` or `y` dies, the reference returned
// must die. The lifetime of the returned reference lasts as long as the
// shortest lifetime of the parameters.
fn largest<'a>(x: &'a i32, y: &'a i32) -> &'a i32 {
    if x > y {
        x
    } else {
        y
    }
}

// The following function is invalid despite the lifetime annotation. Lifetimes
// are about the relation of references, there's no way you could return a
// dangling reference if it's not related to someone else. References as return
// types are like children, you can't let them wander off unless there's an
// adult watching for them - the analogy came out quite good, just ignore the
// fact that we sometimes return the adult by itself just like in `largest`.
// Nevertheless, even if there's a relation between the parameters, Rust
// wouldn't allow such code because there's no derivation of the returned
// reference from the parameters, the lifetimes need to be shared and that means
// that the "child" lifetime must be part of the "parent" lifetime.
// Also, Rust needs an explicit relation between the parameters, it doesn't
// matter if you just return `x` in this function or not unless you have an
// explict lifetime annotation on `x` with the same lifetime as your return type.
// ```
// fn longest<'a>(x: &str, y: &str) -> &'a str {
//     let result = String::from("longest string of them all");
//     result.as_str()
// }
// ```

// Despite what I said about the need for explicit lifetime annotations
// indenpendently if the relation is as clear as just returning the parameter,
// there're some cases that the Rust compiler can infer lifetime annotations.
// The following function adheres to so called __lifetime elision rules__ which
// are a set of particular cases which follow deterministic patterns so the
// compiler can infer the lifetimes relations.
// These patterns are deriving from the lifetime of parameters (input lifetimes)
// and the lifetime on return values (output lifetimes) and consist of three
// rules that the compiler uses. Note that if your code can't be analized based
// on such rules in a way that there's no ambiguity on the result, the Rust
// compiler will throw an error and demand explicit lifetime annotations. The
// rules are:
// 1. A function with n references gets n type annotations
//  - `fn foo(x: &str)` = `fn foo<'a>(x: &'a str)`
//  - `fn foo(x: &str, y: &str)` = `fn foo<'a, 'b>(x: &'a str, y: &'b str)`
// 2. If there's only one input lifetime parameter, its lifetime gets assigned
// to all output parameters
//  - `fn bar(x: &str) -> &str` = `fn bar<'a>(x: &'a str) -> &'a str`
//  - `fn bar(x: &str) -> (&str, &str)` = `fn bar<'a>(x: &'a str) -> (&'a str, &'a str)`
// 3. If there's multiple input parameters but one of them is either `&self` or
// `&mut self`, the lifetime of all output parameters is the same as `&self`
//  - `fn baz(&self, x: &str) -> &str` =~ `fn baz(self: &'a Self, x: &str) -> &'a str`
//  - Note that the example is approximated for brevity, so we don't need `impl`
fn first_word(s: &str) -> &str {
    let bytes = s.as_bytes();

    for (i, &item) in bytes.iter().enumerate() {
        if item == b' ' {
            return &s[0..i];
        }
    }

    &s[..]
}

// It's because of the second rule that we can't implement the following method
// with Rust inferring the lifetimes. Through the application of the first rule
// we get `fn first<'a, 'b>(x: &'a str, y: &'b str) -> &str`, but since there's
// more than one input lifetime parameter, there's no way for the second rule to
// be applied, and the third rule can't be applied because there's no `&self` so
// the output lifetime parameter is unknown
// ```
// fn first(x: &str, y: &str) -> &str {
//     x
// }
// ```

// The behavior of lifetimes on structs is the same as functions, you just need
// to think of the returned reference as being the whole struct and the
// parameters as the fields, which is quite straightforward when you realize
// that a struct is constructed from its fields and not the other way around.
// Thus, as soon as the lifetime of the reference passed to `contents` ends, the
// variable that holds the struct will become invalid because the borrow checker
// connected its lifetime to the contents reference.
#[derive(Debug)]
struct Article<'a> {
    author: &'a str,
    title: &'a str,
    contents: &'a str,
}

// Lifetime annotations on implementations are specified right after `impl` just
// like with generics and they need to be used on the type that the
// implementation refers to. This lifetime annotation is what will be used to
// all the references of `self` on the methods so they turn into
// `fn name(&'b self, ...)`. Along with the lifetime parameter on the
// implementation, we can use lifetime annotations on the method as well if
// needed.
impl<'b> Article<'b> {
    // Here the third lifetime elipsion rule applies because there's a reference
    // to `self` as input lifetime so the output lifetime will be the same as
    // `self`, which follows the impl and is called `'b`. Also, we don't need to
    // annotate the lifetime for `self` because of the first elision rule and
    // that it'll be "infered" from the enclosing `impl`
    fn name(&self, announcement: &str) -> &str {
        println!("{} -> {} by {}", announcement, self.title, self.author);
        self.title
    }

    // I could have established the same relation by using the lifetime 'b on
    // `another` but I choose to use 'c to show the possibility to create
    // lifetime annotations on methods. If we put the same 'b on `another` we
    // wouldn't even need the annotation on the return because of the third
    // elision rule and still achieve the same results
    fn change<'c>(&'c self, another: &'c Article) -> &'c str {
        let other = true;

        if other {
            another.title
        } else {
            self.title
        }
    }
}

// We can combine lifetime annotations with generics without a problem by
// specifying them on the same `<...>`, the only restriction is that lifetime
// parameters must be specified prior to type parameters
fn greatest<'a, T>(x: &'a T, y: &'a T) -> &'a T
where T: PartialOrd
{
    if x > y {
        x
    } else {
        y
    }
}
