// To "publish" a specific path within a module, we have to make the
// item public by itself. As items are private by default, independently
// of their module privacy level, we have to explicitly "publish" them
pub fn add_to_waitlist() {}

fn seat_at_table() {}
