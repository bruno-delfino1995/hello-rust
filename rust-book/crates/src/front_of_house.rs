// We can nest modules however we like to give the enough context/separation
// as needed. Also, we can declare them public of not by using `pub` which
// allows the ancestor module access this module, it allows scopes different
// from the one it was defined to access it. But note that `pub` only
// "publishes" the module and not its contents.
// Here again, we're not providing a body to the module and so Rust will go
// fetch it, but in this case it'll search for `front_of_house/hosting.rs`
// because this is a "submodule" of the `front_of_house` module
pub mod hosting;

// One nice feature of modules in terms of privacy is that by default
// eveything inside its definition is said to be within a privacy boundary,
// so outsiders don't know what lies inside of the module unless allowed
// with the `pub` keyword. This privacy covers modules, structs, functions,
// enums, etc; everything that is inside of the module implementation is
// private by default, and this mechanism works from inside out, the child
// module can reach private paths from the parent module but the other way
// around is invalid. The members defined in parent modules are available
// because the child module knows where it was defined but the parent module
// doesn't know what's defined within the child module
pub mod serving {
    fn take_order() {
        // Also, we can use as many `super` as we want to reach our item
        super::super::back_of_house::cook_order()
    }

    pub fn serve_order() {}

    fn take_payment() {}
}
