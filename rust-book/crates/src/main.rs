// Rust has a set of features tailored to help us better organize our code, from
// modularization to encapsulation; all of these features are said to be part of
// the __module system__, which includes:
//  - packages: cargo feature that let's us test, build, and share crates
//  - crates: a tree of modules that produce either a library or an executable
//  - modules and use: give us control over organization, scope, and privacy of paths
//  - paths: a way of naming items, such as functions, structs, or modules
// With `use` we can bring paths into scope using either an absolute of relative
// path. The result is somewhat like a "symbolic" link with the name as the last
// component of the path that points to the real path. Here we're bringing a
// module from `front_of_house::hosting` into the current module scope just as
// `hosting`.
// You can do this for whatever path you choose, staying at module level or
// going all the way to the function level, but the pattern that arose from the
// community is that we use module when we're going after functions, so we can
// be sure the function isn't defined locally; on the other hand, if we're
// playing with structs, we specify all the way down to the struct/enum name.
use self::front_of_house::hosting;

// This is how we bring the `HashMap` struct from the collections module so it
// can be used here just as `HashMap` and not as `collection::HashMap`. I think
// that we use this way for structs and not for functions because of the
// probability of clashes between those; struct names are much more tailored to
// the meaning the struct holds, while function name derives from many things
// and one of them is the input that may vary to greater degrees
use std::collections::HashMap;

// Sometimes we may choose to only use the module even if we are after a
// specific struct because of name clashes, or we can use the `as` to give
// aliases to what we're importing
use std::fmt::Result;
use std::io::Result as IOResult;

// We can also re-export things by using `pub use`, which would give the
// caller/importer the ability to use the imported module/struct as if it was
// imported along the root module
// The following will allow anyone that have `use crates` to call something like
// `crates::serving::serve_order`
pub use front_of_house::serving;

// We can also clean nested paths like
// ```
// use std::cmp::Ordering;
// use std::fmt;
// ```
// with the following group, which would import both into scope
use std::{fmt, cmp::Ordering};

// And for the cases where you want to import both the whole module and a
// specific struct
// ```
// use std::io;
// use std::io::Write;
// You can use the self to refer back to the "parent" before the block. The
// following will provide both `io::Write` and just `Write` to the current
// module
use std::io::{self, Write};

// Or if you want every fucking public thing from a module, just glob it
use std::collections::*;

fn main() {
    eat_at_restaurant();
}

// By specifying only the module name without a body, we're telling Rust to go
// after a file with the same name as the module and load it for us, this way we
// can separate the modules' code within their own files. Rust now knows that
// there's a module with this name and the content is placed within it own file
// instead of within a block right after the module declaration
mod front_of_house;

// Modules provide organization units to group code within our crates, with them
// we can control not only grouping but also the privacy of paths, such as
// leaving a function (path) for external usage (public) while some struct
// (another path) is only for internal usage (private).
// The declaration of a module is straightforward, you just use a `mod <name> {
// body }` and voila, you have a module with the given name. Just remember that
// modules declared this way are not the root of crates, the roots are
// determined by the conventions we've outlined at `Cargo.toml`, and so the path
// for this module is `crate::front_of_house` because there's an implict module
// named `crate` for the root and these nested modules are all associated to it
mod back_of_house {
    // Structs follow the same principle as modules in which every field within
    // a struct is private by default and you need to explicitly make them public
    pub struct Breakfast {
        pub toast: String,
        seasonal_fruit: String
    }

    // The struct itself is already public, we don't need to make the
    // implementation public because they are linked
    impl Breakfast {
        // If we don't provide such function, the clients that want to
        // instantiate the `Breakfast` because they can't provide concrete
        // values to all of the field of the struct because not all of them are
        // public. But since this is a function associated to the type
        // Breakfast, we can poke the private field without a problem. Also note
        // that this function needs to be public so other modules can access it,
        // the same applies to methods
        pub fn summer(toast: &str) -> Self {
            Breakfast {
                toast: String::from(toast),
                seasonal_fruit: String::from("peach")
            }
        }
    }

    // For enums the story is a little bit different since they aren't really
    // useful unless their variants are public and mostly we're interested in
    // all the variants, so when you make an enum public, all the variants are
    // public as well
    #[derive(Debug)]
    pub enum Appetizer {
        Soup,
        Salad,
    }

    fn fix_incorrect_order() {
        cook_order();

        // As paths start from the module and sometimes we don't really want to
        // go all the way from the root to the module that is just a level above
        // us, Rust provides us with the `super` keyword to work around that
        // annoyance and just do somewhat like a `..` in file systems
        super::front_of_house::serving::serve_order();
    }

    pub fn cook_order() {}
}

fn eat_at_restaurant() {
    // These are known as paths, it's a way to target an specific item from the
    // current scope. They can be structured as absolute paths, through the use
    // of `crate` to start at the beginning, of as relative paths using the
    // current scope as starting point or with `self` and `super` to navigate
    // throughout the modules. The following is an absolute path since it uses
    // the `crate` as root and goes until it hits the desired item, think of
    // crate as `/` in a file system.
    hosting::add_to_waitlist();

    // The decision whether to choose between absolute of relative paths depends
    // on the relations between the code using/importing the path, if they are
    // closely related and tend to move together choose to use relative,
    // otherwise go for absolute.
    // The book shows a preference over absolute paths because it's more likely
    // to move definitions and calls separately
    hosting::add_to_waitlist();

    let mut meal = back_of_house::Breakfast::summer("Wheat");

    // Since `Breakfast.toast` is public, we can do whatever we want with it
    meal.toast = String::from("Rye");

    // But that isn't true for `Breakfast.seasonal_fruit` since we can't even read it
    // println!("The seasonal fruit is {}", meal.seasonal_fruit);

    let soup = back_of_house::Appetizer::Soup;
    let salad = back_of_house::Appetizer::Salad;

    println!("I'm going to eat a {:?} and then a {:?}", salad, soup)
}
