// Rust has borrowed concepts from languages and incorporated on it. Many of
// those come from functional programming languages, such as:
// - closures to store function-like constructs on variables - functions as values
// - iterators for processing series of elements - lazy structures
mod iter;

use std::thread;
use std::time::Duration;

fn main() {
	let name = String::from("String");
	// To force a function to take ownership over its environment we have to use
	// the keyword `move` prior to declaring the closure parameters. With `move`
	// the closure will implement `FnOnce` along with `Fn` because it will take
	// ownership and only needs immutable borrowing. Note that `move` is needed
	// because Rust will just borrow String by default, instead of following the
	// common notion from assignments that move by default
	let greeting = move |greet| format!("{}, {}! - by {}", greet, name, name);
	report(greeting);

	// As `name` was moved, it's the same with ownership rules, the variable
	// can't be used after moved
	// println!("Hello, {}!", name);

	// NOTE: The explanation for function traits is really confusing on the
	// book, which got many people by their ankles:
	// - https://stackoverflow.com/questions/56743984/ownership-closures-fnonce-much-confusion
	// - https://github.com/rust-lang/book/issues/1255
	// - https://stackoverflow.com/questions/30177395/when-does-a-closure-implement-fn-fnmut-and-fnonce
	// And from what I'm seeing, the `FnOnce` doesn't really have anything to do
	// with having ownership over the environment or not. The real deal that it
	// conveys is that you can't call the function more than once. I think that
	// that is the case because the following code is valid and Rust doesn't
	// comply until I call the `FnOnce` more than one time, while the `Fn`
	// variant takes ownership and I can call as many times as I want
	// ```
	// let name = String::from("String");
	// let greeting: Box<dyn FnOnce(String) -> ()> = Box::new(|greet| println!("{}, {}!", greet, name));
	// greeting(String::from("First FnOnce without ownership"));
	// // greeting(String::from("Second FnOnce without ownership"));
	// // If FnOnce really meant ownership over the environment, I wouldn't be able
	// // to do this
	// println!("Hello, {}!", name);

	// let name = String::from("String");
	// let greeting: Box<dyn Fn(String) -> ()> = Box::new(move |greet| println!("{}, {}!", greet, name));
	// greeting(String::from("First Fn with ownership"));
	// greeting(String::from("Second Fn with ownership"));
	// ```

	let name = "str";
	let greet = |prefix| format!("{}, {}!", prefix, name);
	print(greet);

	println!("\n Learning a little about iterators\n");
	iter::main();
}

// As we declare to use `Fn` we can call the function in the body as many times
// as wanted.
fn report<T>(func: T) where T: Fn(String) -> String {
	// NOTE: I didn't use `&str` on the signature because it seemed to need more
	// advanced knowledge
	println!("{}", func(String::from("First")));
	println!("{}", func(String::from("Second")));
}

fn print<T>(func: T) where T: FnOnce(String) -> String {
	println!("{}", func(String::from("First")));
	// As we expect a closure that only implements `FnOnce`, we can call it only
	// once
	// println!("{}", func(String::from("Second")));
}

fn generate_workout(intensity: u32, random_number: u32) {
	// Closures are anonymous functions that capture their context on creation
	// and use it whatever the context when using is. You can create them on a
	// function which would capture values from the function's scope and use it
	// in another function, but you wouldn't lose the referenced variables from
	// the function in which the closure was created.
	// Below we declare a closure that receives one argument and supposedly does
	// some intensive calculation to later return an `i32`. We could have
	// ommited all the types and Rust would still be able to infer them based on
	// usage, so the structure of a closure without types would be just
	// `|param1, param2| { body }` where the braces for the body can be ommited
	// if it's a single expression. The type annotations for closures can be
	// ommited because there's no need to ensure that all clients agree upon
	// some API because closures are usually not exposed through public APIs so
	// Rust is more "open" because there's no need for a clear contract. Only
	// remember that these function must be called for the inference to occur,
	// there's no way Rust can magically know your expected types without some
	// concrete examples - and since Rust types are static, the first call will
	// set the inference for all the following calls
	// The below code results in `expensive_calculation` holding the definition
	// for an anonymous function that can be later used.
	let expensive_calculation = |intensity: u32| -> u32 {
		println!("calculating slowly...");
		thread::sleep(Duration::from_secs(2));
		intensity
	};

	let mut calc = Cacher::new(|intensity: u32| -> u32 {
		println!("calculating slowly...");
		thread::sleep(Duration::from_secs(2));
		intensity
	});

	if intensity < 25 {
		println!(
			"Today, do {} pushups!",
			calc.value(intensity)
		);
		println!(
			"Next, do {} situps!",
			calc.value(intensity)
		);
	} else if random_number == 3 {
			println!("Take a break today! Remember to stay hydrated!");
	} else {
		// As mentioned before, closures are able to capture their surronding
		// environment in exchange for memory overhead. Differently from functions,
		// they are able to refer variables from their upper scope and carry them
		// along until the closure is dropped
		let env_calc = |_ignore| {
			intensity
		};

		println!(
			"Today, run for {} minutes!",
			env_calc(intensity)
		);
	}
}

struct Cacher<T>
// To be able to receive functions as parameters and store them on structs, the
// use of closures in public APIs in general, we have to use type parameters
// with trait bounds that use the trait `Fn` to specify the function signature.
// Since closures are just anonymous functions, we can use them on these APIs
// without a problem, so there's no difference between
// `Cacher { run: std::convert::identity, res: None }` and
// `Cacher { run: |x| x, res: None }` because both comply to the given signature
// (ignore that identity have a type parameter because in the end it'll be i32)
where T: Fn(u32) -> u32
{
	run: T,
	res: Option<u32>
}

impl<T> Cacher<T>
// As the struct has a trait bound, we must provide a T type that complies to
// those "restrictions".
// The traits for functions (Fn, FnOnce, FnMut) distinguish the way that
// closures capture their environment (when upper scope variables are used
// within the body):
// - Fn: borrows the environment values immutably
// - FnMut: can change the environment by mutably borrowing its values
// - FnOnce: takes ownership and can be executed only Once because ownership
//   can't be taken twice
// When creating a closure, these traits are infered based on the usage of the
// environment but it follows some "rules":
// - All implement FnOnce because they can be called at least once
//   - But if the parameter is specified to be FnOnce it means that the functions
//   can be called only once
// - Closures that don't move their captured variables but borrow them mutably
//   implement FnMut
// - Closures that don't need mutable access to captured variables implement Fn
where T: Fn(u32) -> u32 {
	fn new(run: T) -> Self {
		Cacher {
			run,
			res: None
		}
	}

	// This isn't proper memoization because we don't check whether the
	// parameter changed or not, we just give the calculated result
	fn value(&mut self, arg: u32) -> u32 {
		match self.res {
			Some(v) => v,
			None => {
				// Because `self.run` is not a method but a field, we have to
				// "capture" it first either in a variable (which would imply
				// move or copy) or with parenthesis (which just "uses" the
				// field) so we can later call it without Rust getting confused
				// whether it's a field or a method
				let v = (self.run)(arg);
				self.res = Some(v);
				v
			}
		}
	}
}
