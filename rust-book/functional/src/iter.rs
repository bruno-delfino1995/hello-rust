// Iterators abstract the process of iterating over items, much like
// Traversable. In Rust they are lazy and don't execute the computations until
// we consume the results with `consume`, `fold` and other terminating methods.
use std::ops::{Bound, Range, RangeBounds};

pub fn main() {
	let v = vec![1, 2, 3];

	// This is the common way to create iterators, it creates an Iterator
	// without taking ownership over `v` and uses immutable references for so.
	// If we want to take ownership we need to use `into_iter`, and for mutable
	// references, we use `iter_mut`.
	let iter = v.iter();

	// Iterators are what power `for` expressions because they handle all the
	// common logic of having a counter and going from 0 through exaustion by
	// themselves. That logic of knowing whether we need to keep going or we
	// need to stop because we reached the end is implemented on `next`, which
	// has a signature of `fn next(&mut self) -> Option<Self::Item>` inside the
	// `Iterator` trait. From the `next` signature we see that `Iterator`
	// requires us to define a associated type with the trait and that running
	// such method would give us an item or not, and the last part is what let
	// us know when there's nothing more to read. The `mut` from the signature
	// basically exposes a particularity from `Iterator` that they store where
	// it is in the sequence inside of itself
	for el in iter {
		println!("Got {}", el);
	}

	// We have to create another iterator for v because `for` implicitly calls
	// `into_iter` and takes ownership over our little iterator. Which is
	// expected since we use the variable itself in the structure instead of
	// some reference
	let iter = v.iter();

	// Like mentioned before, we're able to use an iterator and construct lazy
	// operations on top of its items until we end it with a __consuming
	// adaptor__, which is a method that calls `next` and runs through the
	// iterator's elements. As these methods consume the iterator, most of them
	// take ownership over the iterator so we cannot reuse an iterator that has
	// reached its end. Once of these methods is `sum` which requires the
	// associated type of the iterator to implement `Sum` so it can reduce the
	// values through "addition"
	let total: i32 = iter.sum();

	assert_eq!(total, 6);

	let iter = v.iter();

	// Another component of the iterator vocabulary are __iterator adaptors__.
	// These are methods that change the current iterator into another type so
	// you can chain multiple lazy computations until you consume your iterator.
	// In this below case we have the `map` iterator adaptor and the `collect`
	// consuming adaptor. Thus creating a lazy computation, that just adds 1 to
	// each of the items, so we can later consume the iterator with every item
	// being run over that by using `collect` to turn it into another collection
	// data type. Note that since iterator adaptors create another iterator
	// without actually running the computation, we must use a consuming adaptor
	// to have the code do something, otherwise it would be just a "dangling"
	// computation
	let inc: Vec<i32> = iter.map(|x| x + 1).collect();

	let counter = Counter::new(1..4).unwrap();

	for n in counter {
		println!("It's {}", n);
	}

	let c1 = Counter::new(1..4).unwrap();
	let c2 = Counter::new(1..4).unwrap();

	// As the minimal implementation for `Iterator` requires only `next`, we get
	// all the other methods because of their default implementations
	let sum: u32 = c1
		.zip(c2.skip(1))
		.map(|(a, b)| a * b)
		.filter(|x| x % 3 == 0)
		.sum();

	println!("The sum is {}", sum);

}

struct Counter {
	start: u32,
	curr: u32,
	end: u32,
}

impl Counter {
	fn new(range: Range<u32>) -> Option<Counter> {
		let start = match range.start_bound() {
			Bound::Included(v) => *v,
			Bound::Excluded(v) => v + 1,
			_ => 0,
		};

		let end = match range.end_bound() {
			Bound::Included(v) => v + 1,
			Bound::Excluded(v) => *v,
			_ => 0,
		};

		if start >= end {
			None
		} else {
			Some(Counter {
				start,
				end,
				curr: start,
			})
		}
	}
}

// We can implement Iterator for any type than can be "runned over"
impl Iterator for Counter {
	type Item = u32;

	fn next(&mut self) -> Option<Self::Item> {
		if self.curr < self.end {
			let curr = self.curr;
			self.curr += 1;
			Some(curr)
		} else {
			None
		}
	}
}
