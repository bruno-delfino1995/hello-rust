use std::collections::HashMap;
use std::env;
use std::process;

// To use our tested library crate, we must import it by using the same name as
// the package and specifying the path for what we want
use minigrep::{Args, Toggle};

fn main() {
    // As the main function doesn't receive arguments for CLI parameters, like
    // many languages, we've to resort to `env::args` to help us on gathering
    // those within an iterator. Also, to prevent us from string bugs, it panics
    // on invalid Unicode, so if you need that you would use `std::env::args_os`
    // that returns an iterator of `OsString` instead of `String`.
    let args = env::args();
    let toggles = env_toggles();
    let args = Args::new(args, &toggles).unwrap_or_else(|err| {
        // `eprintln!` is the `println!` for standard error that comes with the
        // std library and is included by default with the Prelude
        eprintln!("Problem parsing arguments: {}", err);
        // As `process::exit` has the never type `!` as its return, meaning that
        // it's a function that never returns because you can't create values
        // for the never type, combined with the fact that the never type can be
        // coerced into any other value, there's no problem on returning it.
        // Also, the `process::exit` is said to never return because it moves
        // the control flow, like `continue` and `break`, by exiting the program
        // and returning the argument as the exit code
        process::exit(1)
    });

    // As we don't care about the success case, there's no reason to use match
    // or another option that allows us to know that it succeeded. Also, `run`
    // doesn't really return a value because its Ok variant holds nothing but a
    // `Unit`
    if let Err(e) = minigrep::run(args) {
        eprintln!("Application error: {}", e);
        process::exit(1);
    }
}

fn env_toggles() -> HashMap<Toggle, bool> {
    let mut envs = HashMap::new();

    envs.insert(
        Toggle::Case,
        // Here we use `is_err` because we only want to know whether the
        // variable is set or not and set it to the inverse of its meaning if it
        // were set
        env::var("CASE_INSENSITIVE").is_err(),
    );

    envs
}
