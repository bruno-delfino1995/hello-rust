//! Library used by the CLI
//!
//! This follows the pattern of having a separate library for the logic code
//! instead of bundling everything inside of the binary crate

// The comment above is another type of documentation comment. However, instead
// of documenting the following code, it documents the code that contains it.
// It's a doc comment useful for documenting crates and modules because they
// don't have a "starting code" like functions and structs where we attach the
// documentation to the declaration line

use std::collections::HashMap;
use std::env;
use std::error::Error;
use std::fs;
use std::hash::Hash;

pub struct Args {
    pub query: String,
    pub filename: String,
    pub case_sensitive: bool,
}

// I used this enum of toggles to give a boundary to what can be toggled by
// environment variables and also to keep this code away from the environment as
// much as possible
#[derive(Eq, PartialEq, Hash, Debug)]
pub enum Toggle {
    Case,
}

impl Args {
    pub fn new(
        mut args: env::Args,
        env_toggles: &HashMap<Toggle, bool>,
    ) -> Result<Self, &'static str> {
        // Like many platforms, the `args[0]` is reserved to the name/path of the
        // executable
        args.next();

        let query = match args.next() {
            Some(arg) => arg,
            None => return Err("Didn't get a query string"),
        };

        let filename = match args.next() {
            Some(arg) => arg,
            None => return Err("Didn't get a file name"),
        };

        let case_sensitive = env_toggles.get(&Toggle::Case).unwrap_or(&false);

        Ok(Args {
            query,
            filename,
            case_sensitive: *case_sensitive,
        })
    }
}

// This is a pattern to separate concerns on binary projects where we separate
// the program logic into a function named `run` into the library crate of the
// corresponding binary crate. It keeps the code on main slim enough to be
// inspected by eye and allows the library code to be tested with integration
// tests
pub fn run(args: Args) -> Result<(), Box<dyn Error>> {
    let contents = fs::read_to_string(&args.filename)?;
    let searcher = if args.case_sensitive { search } else { isearch };

    for line in searcher(&args.query, &contents) {
        println!("{}", line);
    }

    Ok(())
}

fn search<'a>(query: &str, contents: &'a str) -> Vec<&'a str> {
    // Rust has a "concept" of zero-cost abstractions, which means that
    // abstractions impose no additional runtime overhead, so this code with
    // iterators should perform the same as a code more imperative with `for`.
    // Sometimes this feature can be seen by looking at different codes with the
    // same "mechanics" that boil down to the same assembly code
    // https://medium.com/ingeniouslysimple/rust-zero-cost-abstraction-in-action-9e4e2f8bf5a
    contents.lines().filter(|l| l.contains(query)).collect()
}

fn isearch<'a>(query: &str, contents: &'a str) -> Vec<&'a str> {
    let query = query.to_lowercase();

    contents
        .lines()
        .filter(|l| l.to_lowercase().contains(&query))
        .collect()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn one_result() {
        let query = "duct";
        let contents = "\
Rust:
safe, fast, productive.
Pick three.";

        assert_eq!(vec!["safe, fast, productive."], search(query, contents));
    }

    #[test]
    fn insensitive_search() {
        let query = "p";
        let contents = "\
Rust:
safe, fast, productive.
Pick three.";

        assert_eq!(
            vec!["safe, fast, productive.", "Pick three."],
            isearch(query, contents)
        );
    }
}
