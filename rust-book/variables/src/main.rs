fn main() {
    println!("Eleventh fibonacci element is {}", nth_fibonnaci(11));
    println!("55 Fahrenheit is {} Celsius", fahrenheit_to_celsius(55));
    println!("\nHere goes a poem\n");

    twelve_days_of_christmas();

    println!("Now some learning messages\n");
    learning();
}

// Explicity allow unused variables. This allow you to remove the `_` from the
// beginning of an unused variable without getting the compiler to scream at you
#[allow(unused_variables)]
fn learning() {
    say_hello_world();
    // Differently from some languages where a variable declaration is an
    // expression, in Rust it's not since it won't return the value that was
    // assigned to it. Statements are instructions that perform some action and
    // do not return a value. Expressions evaluate to a resulting value. Given
    // that difference, variable declaration falls into statements since it
    // doesn't return a value while some things as conditional - if to be
    // precise - is a expression since it returns the value that the matching
    // body returns.
    let a = 1;
    // As Rust have strong typing and the evaluation of the last expression on a
    // block will determine the result for that block, we have to ensure that
    // all paths share the same type. Otherwise, Rust will complain because it
    // cannot determine a single type for the variable. You might think that
    // since the value used by if is static Rust could be smart and infer it,
    // however, it'd make the compiler way more complex than needed for a
    // frivolous check.
    let b = if a > 1 { true } else { false };

    let mut counter = 0;
    // For running something indefinitely we can use the `loop` construct and
    // it'll repeat its body until the program itself receives a signal to stop
    // or if within the body there's a `break` expression. But oddly enough,
    // `loop` is an expression with the result being a value passed to `break`,
    // thus we can use it on assignments since it's an expression and return a
    // value
    let result = loop {
        counter += 1;
        if counter == 10 {
            // The value returned by loop expressions is determined by the
            // value provided to break when stoping
            break counter * 2;
        }
    };

    println!("The value is {}", result);

    let arr = [10, 20, 30, 40, 50];
    let mut i = 0;
    while i < 5 {
        println!("array element is {}", arr[i]);
        i += 1;
    }

    let mut vec = vec![10, 20, 30, 40, 50];

    // Besides the usual `i < arr.len` we can use pattern matching as
    // "conditions" on while loops to stop as soon as they fail. Using this
    // approach prevents errors such as forgetting to update the value used as
    // counter or updating it beyond the array/vector length
    while let Some(el) = vec.pop() {
        println!("vec element is {}", el);
    }

    // To easen everything when it comes to running through data structures,
    // which is abstracted by iterators in most languages, we've got the for
    // construct. Differently from the usual, it doesn't have the construct of
    // `init;cond;inc`, instead it uses the iterator abstraction to run through
    // its exaustion.
    for el in arr.iter() {
        println!("iter element is {}", el);
    }

    // We can use the `Range` type to create an iterator for counting and put it
    // together with `for` to implement the "from x to y"
    for n in 1..6 {
        println!("the number is {}", n);
    }

    // By default variables are immutable in Rust, it's this way because
    // explicit mutability makes the code more easy to reason since you don't
    // have to guess who is changing what. And by using immutability by default
    // we protect ourselves since mutability can bring many problems, since the
    // tracking of it within the flow can be difficult, aka changing a variable
    // inside an if statement, when programmers aren't aware of it.
    let x: u16 = 5;
    show_value(x as i32);

    // This will fail because x doesn't have the `mut` modifier to enable
    // mutability on it
    // x = 6;
    // println!("The value of x is: {}", x);

    // However, we have variable rebinding, like in Elixir, this simply shadows
    // the previous value and allows us to reuse the name with a different value
    // from now on, even allowing us to derive this new value from the previous.
    // This is called Shadowing.
    let x: f32 = add(x as i32, 2) as f32;
    println!("The value of x is: {}", x);

    // To turn a variable into a mutable binding, you just add a `mut` modifier
    // when declaring it
    let mut y = 5;
    println!("The value of y is: {}", y);

    // Note that differently from shadowing, we cannot change the variable type,
    // otherwise rust will complain
    y = 6;
    println!("The value of y is: {}", y);

    // Immutable variables may remind you of constants; however, in Rust they
    // have some subtle differences. Constants are declared using `const` along
    // with the type annotation and a constant expression that computes to its
    // value. This expressions that represents its value is the main difference
    // between constants and variables, because constant values cannot be derived
    // from something that only can be computed during runtime.
    const MAX_POINTS: u32 = 100_000;
    if y >= MAX_POINTS {
        println!("You've won!!");
    }
    // Constants are a nice replacement for hard-coded values as they seem to
    // have no impact on performance and provide a better reasoning for the
    // value

    // As Rust is a statically typed language with type inference, you better
    // know about types when you don't want Rust doing it for you. In Rust,
    // there're two type subsets: scalar and compound types; within scalars we
    // have: integers, floating-point numbers, booleans, and characters; and for
    // compound we have the two primitives: tuples and arrays.

    // Integer: number without fractional component. It's represented by whether
    // you want it to be signed and how much space you want to reserve for it;
    // you use `i` for signed and `u` for unsigned, followed by `8-128` for how
    // many bits you want. Also, you can ignore the size and let rust derive it
    // from the system's architecture by using `arch` - 32bits for x86 and
    // 64bits for x64. By default, Rust will choose `i32` for your integers since
    // it's generally the fastest even on x64 architectures. Besides all this
    // for declaring the type, you can use the following bases to declare an
    // integer
    let decimal = 98_222;
    let hex = 0xff;
    let octal = 0o77;
    let binary = 0b1111_0000;
    let _byte = b'A';

    // Floating-Point: for floating point types rust is a little bit more
    // limited, allowing only the usage of `f32` and `f64`, with `f64` being the
    // default since it's roughly the same speed as `f32` but with more
    // precision. Also, these types follow IEEE-754 so `f32` is single precision
    // and `f64` is double precision
    let precision = 2.0; // f64
    let rough: f32 = 3.0;

    // Boolean: declared using `bool` and having `true` and `false` as its values
    let t = true;
    let f: bool = false;

    // Character: much like C, rust suppor character values using `char` and
    // putting a single letter inside single quotes. It has a size of four bytes
    // and represents a Unicode Scalar Value, thus we're able to use emoji and
    // chinese characters. It ranges from `U+0000` to `U+D7FF` and `U+E000` to
    // `U+10FFFF` inclusive.
    let a = 'A';
    let b = 'b';
    let heart_eyed_cat: char = '😻';

    // Tuple: heterogeneous collection with fixed size. That means we use it
    // to declared a compound type with pieces of different types, but once
    // declared it cannot grow or shrink its size.
    let pair = (1.0, 2.0);
    let tup = (true, false, 1, 0);
    let tup: (i32, f64, char, bool) = (1, 2.0, 'C', true);

    // We can destruct tuples by using the same structure using to declare it
    // but on the names side instead of the values side, or for the
    // knowledgeable reader, using pattern matching
    let (x, y) = pair;
    println!("You are at: {}, {}", x, y);

    // But you don't need to destruct the tuple to get a single value, you can
    // access them by their indexes
    println!("You are at coordinates: {}, {}", pair.0, pair.1);

    // Arrays: homogeneous collection with fixed size. Unlike more dinamic
    // languages, where an array can grow and shink, rust follows what the C way
    // for static arrays, for dinamic ones you would need to declare them
    // differently. Given their static characteristics, they are allocated in
    // the stack instead of the heap, and you have to declare its size upfront
    // if you want to be declaring its type.
    let arr: [i32; 4] = [1, 2, 3, 4];
    let all_three: [i32; 3] = [3; 3];

    // Acessing an array looks exactly like C, and much like C, it panics when
    // out of bounds
    println!("The value of element is: {}", arr[3])
    // println!("The value of element is: {}", all_three[3]); // you should start counting at 0
}

// Basic function declarations in rust consist of
// `fn <name>([...<arg>: <type>]) -> <return_type> { ... }`, and are called as
// many languages with `<name>([...params])`. You don't need to worry whether to
// declare your function before using it because Rust doesn't care about that,
// it just needs to be available in the scope.
fn say_hello_world() {
    println!("Hello World!");
}

// Function parameters shows the inputs of the function and are enclosed within
// the parenthesis on the declaration. They are like a variable declaration but
// without `let` or `const`, but the name and type annotation are required.
// Parameters enable function callers to provide arguments of which the function
// will use for its work.
fn show_value(x: i32) {
    println!("The value of x is: {}", x)
}

// For the return side, we just need to say which value it's and we're all set.
fn add(x: i32, y: i32) -> i32 {
    // return x + y;
    let sum = x + y;

    let fat = {
        let x = 3;
        x + 1
    };

    // In case we don't terminate the expression with `;` it's returned by
    // default, because the return value is synonymous to the value of the final
    // expression in the block of the body. This is used on `if` instead of
    // return because return would terminate the function while the lack of a
    // semicolon indicates the "return" for a given block. The addition of the
    // semicolon at the end of an expression you turn it into an expression and
    // thus you have no value returning from it, so if we add a `;` to the
    // expression `x + y` on the block that defines fat, fat would be `()`
    // instead of `i32` because the block didn't have a "return".
    sum + fat
}

fn nth_fibonnaci(i: u32) -> u32 {
    if i == 1 || i == 2 {
        return 1;
    }

    let mut previous = 1;
    let mut current = 1;
    for _ in 2..i {
        let cache = current;
        current = previous + cache;
        previous = cache;
    }

    return current;
}

fn fahrenheit_to_celsius(fh: i32) -> f32 {
    return ((fh - 32) as f32) * (5.0 / 9.0);
}

fn twelve_days_of_christmas() {
    let gifts = [
        "a partridge in a pear tree",
        "two turtle doves",
        "three French hens",
        "four calling birds",
        "five gold rings, badam-pam-pam",
        "six geese a laying",
        "seven swans a swimming",
        "eight maids a milking",
        "nine ladies dancing",
        "ten lords a leaping",
        "eleven pipers piping",
        "12 drummers drumming",
    ];

    let days = [
        "first",
        "second",
        "third",
        "fourth",
        "fifth",
        "sixth",
        "seventh",
        "eighth",
        "ninth",
        "tenth",
        "eleventh",
        "twelfth",
    ];

    for day in 1..13 {
        println!("On the {} day of Christmas", days[day - 1]);
        println!("My true love gave to me");

        for verse in (0..day).rev() {
            let gift = if day > 1 && verse == 0 {
                "and ".to_owned() + gifts[verse]
            } else {
                gifts[verse].to_owned()
            };

            println!("{}", sentence_case(gift))
        }

        println!("");
    }


}

fn sentence_case(text: String) -> String {
    let mut c = text.chars();
    match c.next() {
        None => String::new(),
        Some(f) => f.to_uppercase().collect::<String>() + c.as_str(),
    }
}