mod internal_mutability;
mod cycle;
// Smart pointers are somewhat related to normal references (& is the most
// primitive type of pointer), in a manner that they both reference some piece
// of data stored elsewhere in the memory, be it stack or heap. However, these
// pointers have some metadata and capabilities that give them superpowers. But
// sometimes smart pointer actually own the data that they point to, setting
// them apart from references that always borrow that data.
//
// One commont type of smart pointer that we've been using are Strings and
// Vectors, which both own memory and allow you to manipulate it while keeping
// metadata for things such as their capacity or having capabilities such as
// guaranteeing that all the data inside of it is a valid UTF-8 character.
//
// You can implement your own smart pointer beyond the ones we will be exploring
// since most of them are structs that implement `Deref` and `Drop`, which,
// respectively, allow the struct to behave like a plain reference and to
// customize the code that's executed before the memory gets freed

// As Rust needs to know the size of the data the program will be manipulating
// at compile time, we can't create a normal _cons list_ due to the infinite
// type recursion on the Cons side which leads to an unknown size. To counter
// this we can wrap the recursion with a type that adds some indirections to the
// target data but in doing so it adds a known size to that part of the whole
// struct. This indirection helps Rust calculate the size because a pointer size
// doesn't change depending on the data that it refers to, we get a known size
// to work with. It won't spiral down trying to guess what size is the Cons
// variant and stop at pointer, thus the size of List variables will be the
// biggest variant of this enum.
#[derive(Debug)]
enum List {
	// Cons(i32, List),
	Cons(i32, Box<List>),
	Nil,
}

#[derive(Debug)]
enum RList<'a> {
	RCons(i32, &'a RList<'a>),
	RNil
}

// Rc is not part of prelude as its uses are more specific than what a simple
// Box, which is required more frequently due to trait objects
use std::rc::Rc;

#[derive(Debug)]
enum SList {
	SCons(i32, Rc<SList>),
	SNil
}

// As the current module only knows about the List and not its variants, we need
// to explicitly call `use` to make them available to our code. Without this
// we'd use `List::Cons` instead of just `Cons`
use crate::List::*;
use crate::RList::*;
use crate::SList::*;

struct Pack<T: std::fmt::Display>(T);

impl<T: std::fmt::Display> Pack<T> {
	fn new(value: T) -> Pack<T> {
		Pack(value)
	}
}

use std::ops::Deref;

// Deref allows us to override the `*` operator when implementing `deref` for a
// target of our choice. If we plan to work on mutable references we need its
// cousing `DerefMut` that will be able to convert between mutable references
// from T to U. Also, with simple Deref we convert a mutable reference to
// an immutable one but not the other way around, even with DerefMut, because
// Rust borrowing rules don't allow it since they cannot guarantee that there
// won't be extra immutable references to the same data that generated the
// mutable reference. ~~This coercion from `&mut T` to `&U` is possible because
// Rust prevents that two mutable references to the same exist at the same time
// before the conversion while it doesn't do so with immutable references~~
impl<T: std::fmt::Display> Deref for Pack<T> {
	type Target = T;

	// This is what the deref does, it just gets your type and gives something
	// else in return. As the compiler only knows how to work with plain
	// references, we need to return one of those so it can actually follow the
	// pointer to a value when doing `*(x.deref())` behind the scenes. Another
	// reason to return references instead of plain values is due to ownership
	// because we don't want to move the value outside of self when dereferencing
	// it.
	fn deref(&self) -> &Self::Target {
    &self.0
	}
}

// Drop is the second piece to a full smart-pointer. It's the trait used by Rust
// to free your resources once they've gone out of scope or explicitly freed by
// using `drop(p)`. Rust will automatically place/call this code after your
// resource is no longer needed.
impl<T: std::fmt::Display> Drop for Pack<T> {
	fn drop(&mut self) {
		println!("Dropping our smart pointer {}!", self.0)
	}
}

fn hello(name: &str) {
	println!("Hello dear {}!", name)
}

fn main() {
	// Boxes are one of the most fundamental smart pointers, they allow us to
	// store data on the heap instead of the stack. Here we're simply telling that
	// `b` is a pointer that has its data stored on the heap, and we can print
	// it without a problem because smart pointers are just normal structs
	let b = Box::new(5);
	println!("b = {}", b);

	// The downside to this whole indirection is when instantiating a new list,
	// but I'm ok with that based on the guarantees on memory that Rust provides
	// in exchange
	let list = Cons(1, Box::new(Cons(2, Box::new(Cons(3, Box::new(Nil))))));
	println!("list = {:?}", list);

	// Another use of the Box smart pointer is to create a type that cares only
	// about which traits its type param holds. We saw this when dealing with
	// errors on `main` by using `Box<dyn Error>`, effectively creating a type
	// that cares only whether the inner type implements the Error trait

	let value = 5;
	let reference = &value;
	let b = Box::new(value);
	let p = Pack::new(value);

	println!("{}", value);
	// The dereference operator (*) is what let us navigate from the pointer to
	// the value pointed at, thus following the reference. It's implemented for
	// plain references created by `&x` and implemented for custom structs through
	// the `Deref` trait
	println!("{}", *reference);
	println!("{}", *b);
	// Given that we implemented Deref for `Pack` we can use the dereference
	// operator as if it were a normal reference
	println!("{}", *p);

	// Deref is sort of a "conversion" from one type to its inner data, and
	// because of that feature Rust can coerce it indefinitely until your type
	// matches the expected type. When working with references we don't have to
	// worry whether we're using plain references or smart pointers because
	// through **Implicit Deref Coercion** Rust will call deref as many times as
	// needed until it match the parameters.
	let name = Pack::new("Hoare");
	hello(&name);

	// Don't be fooled thinking that rust only knows how to deref on parameters.
	// Rust is a smart boy that knows its types well and can't wait to help you
	// managing one when it sees them.
	let plain: &str = &name;
	hello(plain);

	// If Rust didn't have coercion we would need to convert the types ourselves
	// by first reaching out to the `String` within `Pack<String>` by
	// dereferencing it with `*`. Once with a good old String we can transform it
	// into a slice with `[..]` that can be referenced with & at last.
	hello(&(*name)[..]);

	// Due to how Rust constrols memory based on scopes/lifetimes we can't
	// call the destructor method (`drop`) directly from the instance because the
	// variable would still be valid and Rust would try to free it once its
	// lifetime ends. This would cause a double free problem because we're calling
	// `instance.drop()` explicitly and Rust is calling `std::mem::drop(instance)`
	// implicitly. Also, due to being a method that acts on a mutable reference to
	// self instead of an plain old value, if we could call `.drop` from a
	// reference we would be freeing memory that's still owned somewhere else,
	// which shows how ingineous is the drop method since all it does is just
	// receving an owned data and never returning it, thus telling Rust that the
	// data should be dropped once it's impossible to access it again due to the
	// ownership system
	// name.drop();
	drop(name);

	// One may say that smart-pointers are useless, all you need are references
	// and lifetimes. But stop to think on how you make a list that can be shared
	// partially to save memory, like the "persistent data structures". By using
	// just lifetimes you would link the tail of the list to its head, and
	// since its head is going to be shared as tail for another list with a
	// different head, you would end up with a list that its all linked correctly.
	let a = RCons(5, &RCons(10, &RNil));
	let b = RCons(3, &a);
	let c = RCons(4, &a);

	println!("b = {:?}", b);
	println!("c = {:?}", c);

	// It's not all wrong to use references with lifetimes, but maybe there're
	// easier and that will allocate your memory on the heap such as Rc. The only
	// downside to Rc is that you need to create the value that's supposed to be
	// shared within an Rc, meanwhile, with references you don't need to do so.
	let a = Rc::new(SCons(5, Rc::new(SCons(10, Rc::new(SNil)))));
	println!("The count starts at {}", Rc::strong_count(&a));

	// Rc stands for Reference Counting and when cloning it we're only
	// incrementing the count. That's why it's Rust's convention to use Rc::clone
	// instead of x.clone() because the latter may lead the developer into
	// thinking that a costly full memory copy is going to happen
	let b = SCons(3, Rc::clone(&a));
	println!("Increments when cloned {}", Rc::strong_count(&a));
	println!("b = {:?}", b);

	{
		let c = SCons(4, Rc::clone(&a));
		println!("No matter how many times clonned {}", Rc::strong_count(&a));
		println!("c = {:?}", c);
	}
	// Given that an instance was dropped, Rc's Drop implementation is smart
	// enough to decrement the count from the original Rc
	println!("Tracks drops accordingly {}", Rc::strong_count(&a));

	internal_mutability::main();
	cycle::main();
}
