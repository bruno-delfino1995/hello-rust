// Sometimes we need to mutate data internally while providing an API surface
// that seems immutable, such as when implementing a spy for a trait which has
// all the methods with `&self` instead of `&mut self`. In times like these we
// want to escape the compile time borrowing rules to mutate a variable that's
// internal to our type and for that purpose there's `RefCell`, which is a smart
// pointer capable of bending the rules at compile time while maintaining the
// same during runtime.

// With RefCell we have a type with single ownership, much like Box, but that
// provides methods capable of borrowing the data at runtime without the
// compiler's knowledge. Such methods still report back to the owner and help it
// keep tracking of borrowing during runtime, thus if we violate rules such as
// having more than one mutable reference, or having one mutable reference while
// holding an immutable reference, the owner will catch such scenarios and panic
// at runtime.
use std::cell::RefCell;
use std::rc::Rc;

// Internally Mutable List. By combining Rc with RefCell we enable multiple
// owners of mutable data, which means that we can share our references with
// whomever we need while being able to mutate it internally. For our internally
// mutable list we could use a reference in the value side without a problem,
// but this would incurr a lifetime since we can't have memory dangling around,
// and this doesn't happen when we use Rc because of its reference tracking
#[derive(Debug)]
enum IMList {
	Cons(Rc<RefCell<i32>>, Rc<IMList>),
	Nil,
}

use self::IMList::*;

pub fn main() {
	let _x = 5;
	// Without the power of RefCell, we can't create mutable references from
	// immutable values because of the borrowing rules that guarantee that once a
	// value is declared immutable all of its references must follow
	// let y = &mut x;

	let x = RefCell::new(5);
	println!("x = {:?}", x);

	// Because of RefCell and it's internal usage of `unsafe` to bypass most of
	// the rules enforced by Rust, we can safely borrow a mutable reference to a
	// value without denoting that the owner supports mutability
	let mut y = x.borrow_mut();
	*y = 10;

	// As the borrowing rules are enforced during runtime, we can't see the value
	// within because it would mean that it's possible to have a immutable
	// reference while there's a mutable reference, which would allow data races
	// to happen. To prevent this, the debug implementation for RefCell will omit
	// the value and if we try to borrow the value itself to check for its
	// internal value we'd panic at runtime
	println!("x = {:?}", x);
	// println!("x = {}", x.borrow());

	// However, since RefCell is a smart-pointer that correctly keeps track of its
	// references, all we have to do is drop the mutable reference to allow a new
	// immutable borrow to happen
	drop(y);
	println!("x = {}", x.borrow());

	// Once we have this reference that'll be later clone, we are able to mutate a
	// value within the list without having to change the whole list.
	let value = Rc::new(RefCell::new(5));

	// But don't assume that by having different owners that we'll bypass the rules
	// from RefCell, the value within is the same despite the Rc being shared many
	// times
	// let another_owner = Rc::clone(&value);
	// let immutable_ref = another_owner.borrow();
	// let other_owner = Rc::clone(&value);
	// This call will fail because there's an immutable reference/borrow for the
	// same RefCell before you create this new reference/borrow
	// let mutable_ref = other_owner.borrow_mut();

	let a = Rc::new(Cons(Rc::clone(&value), Rc::new(Nil)));

	let b = Cons(Rc::new(RefCell::new(3)), Rc::clone(&a));
	let c = Cons(Rc::new(RefCell::new(4)), Rc::clone(&a));

	// Remember that Rust also have automatic referencing which automatically
	// reaches for the references when it finds a method with a signature that
	// can't be fulfilled by the current type but can by a type that it refers to.
	// The following is converted to *(&value).borrow_mut() because Rc doesn't
	// have any `borrow_mut` but `RefCell` which is referenced by Rc does. Also,
	// by not attributing this borrow to a variable we're creating a reference and
	// dropping it right after the attribution is executed, so there's no problem
	// if you create a new borrow afterwards
	*value.borrow_mut() += 10;

	println!("a after = {:?}", a);
	println!("b after = {:?}", b);
	println!("c after = {:?}", c);
}

// Below you can see the mocking usage described where we have a trait that's
// completely immutable, but to create a spy for this cooperator we need to
// implement the internal tracking using RefCell to enable mutability on a value
// that's externally immutable
pub trait Messenger {
    fn send(&self, msg: &str);
}

pub struct LimitTracker<'a, T: Messenger> {
    messenger: &'a T,
    value: usize,
    max: usize,
}

impl<'a, T> LimitTracker<'a, T>
where
    T: Messenger,
{
    pub fn new(messenger: &T, max: usize) -> LimitTracker<T> {
        LimitTracker {
            messenger,
            value: 0,
            max,
        }
    }

    pub fn set_value(&mut self, value: usize) {
        self.value = value;

        let percentage_of_max = self.value as f64 / self.max as f64;

        if percentage_of_max >= 1.0 {
            self.messenger.send("Error: You are over your quota!");
        } else if percentage_of_max >= 0.9 {
            self.messenger
                .send("Urgent warning: You've used up over 90% of your quota!");
        } else if percentage_of_max >= 0.75 {
            self.messenger
                .send("Warning: You've used up over 75% of your quota!");
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::cell::RefCell;

    struct MockMessenger {
        sent_messages: RefCell<Vec<String>>,
    }

    impl MockMessenger {
        fn new() -> MockMessenger {
            MockMessenger {
                sent_messages: RefCell::new(vec![]),
            }
        }
    }

    impl Messenger for MockMessenger {
        fn send(&self, message: &str) {
            self.sent_messages.borrow_mut().push(String::from(message));
        }
    }

    #[test]
    fn it_sends_an_over_75_percent_warning_message() {
        let mock_messenger = MockMessenger::new();
        let mut limit_tracker = LimitTracker::new(&mock_messenger, 100);

        limit_tracker.set_value(80);

        assert_eq!(mock_messenger.sent_messages.borrow().len(), 1);
    }
}
