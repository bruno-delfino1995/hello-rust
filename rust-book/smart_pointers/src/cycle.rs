use std::borrow::BorrowMut;
use std::cell::RefCell;
use std::rc::{Rc, Weak};

#[derive(Debug)]
enum List {
	// By using RefCell we enable the client the change the tails content without
	// having to report back to the owner, and with Rc we let multiple immutable
	// owners of that data
	Cons(i32, RefCell<Rc<List>>),
	Nil,
}

use self::List::*;

impl List {
	fn tail(&self) -> Option<&RefCell<Rc<List>>> {
		match self {
			Cons(_, tail) => Some(tail),
			Nil => None
		}
	}
}

#[derive(Debug)]
struct Node {
	value: i32,
	children: RefCell<Vec<Rc<Node>>>,
	// As we don't want ownership over the parent because this would cause a
	// relationship that a death of a single child could kill one parent, and that
	// also it doesn't make any sense for the leaf in a tree to kill its branch,
	// we're using a weak reference so the parent can move on without a problem if
	// we have a variable. But most importantly, if we were to reference the
	// parent with a common Rc we would be opening up for reference cycles and
	// further memory leaks because the reference counts would never reach 0
	parent: RefCell<Weak<Node>>,
}

pub fn main() {
	let a = Rc::new(Cons(5, RefCell::new(Rc::new(Nil))));

	println!("a initial rc count = {}", Rc::strong_count(&a));
	println!("a next item = {:?}", a.tail());

	let b = Rc::new(Cons(10, RefCell::new(Rc::clone(&a))));

	println!("a rc count after b creation = {}", Rc::strong_count(&a));
	println!("b initial rc count = {}", Rc::strong_count(&b));
	println!("b next item = {:?}", b.tail());

	if let Some(link) = a.tail() {
		// Here we're replacing a's tail value by a clone of b and thus creating a
		// cycle because b's tail is a. This drops the old tail from a that had a Rc
		// with Nil as value and it's properly dropped
		*link.borrow_mut() = Rc::clone(&b);
	}

	println!("b rc count after changing a = {}", Rc::strong_count(&b));
	println!("a rc count after changing a = {}", Rc::strong_count(&a));

	// Because b's Rc is pointed by both a's tail and b as owner, it's count is 2
	// and the same goes for a's Rc but the other way around. Due to this setup we
	// create a memory leak because when each variable is dropped they reduce
	// their respective Rc's count by 1, however since both tails point at each
	// other we never get the change to fully clean memory.
	// a(5 -> b(10 -> a))
	// As Rust know only how to automatically drop owners and the rest follows if
	// they end having a Rc count of 0, but in this case Rust only know about a
	// and b owners but both have different heads where each have a count of 2,
	// there's no way that their tails will be dropped because we need to reach 0.
	// That will never happen because once b and a are dropped they each remove
	// one of the references being counted, but their values still point to each
	// other.
	// All that you need to do is first clean the tail before dropping the head,
	// which can be done either manually or by your Drop implementation in a
	// manner that you start by removing the dust behind you before removing
	// yourself, but the latter is a lot more complex because it can open more
	// corner cases
	if let Some(link) = b.tail() {
		// Now we decrease the count of a's value to 1, which will allow it to clean
		// everything once both owners are removed because one of the heads will
		// reach 0 and start the drop cascade
		*link.borrow_mut() = Rc::new(Nil)
	}

	you_do_not_own_meeee_because_youre_weak()
}


// One option to circle around reference cycles is to stop expressing
// ownership when using `Rc`, because if there're multiple owners Rc will wait
// until all of them cease to exist. However, if you choose to create a
// non-ownership relationship, somewhat like sharing without caring about who
// you're sharing with, which is possible by using weak references created by
// `Rc::downgrade`. This method will create a `Weak<T>` that increments the
// `weak_count` of the original owner instead of the `strong_count`, and since
// it's a sharing that the original owner doesn't care, it doesn't keep the
// original memory data from being freed because only `strong_count` needs to
// reach 0, because of this uncertainty whether the actual data exists that
// most methods inside a `Weak<T>` will return an `Option<Rc<T>>` so you can handle it
fn you_do_not_own_meeee_because_youre_weak() {
	let leaf = Rc::new(Node {
		value: 3,
		parent: RefCell::new(Weak::new()),
		children: RefCell::new(vec![]),
	});

	println!(
		"leaf strong = {}, weak = {}",
		Rc::strong_count(&leaf),
		Rc::weak_count(&leaf),
	);

	{
		let branch = Rc::new(Node {
			value: 5,
			parent: RefCell::new(Weak::new()),
			children: RefCell::new(vec![Rc::clone(&leaf)]),
		});

		*leaf.parent.borrow_mut() = Rc::downgrade(&branch);

		println!(
			"branch strong = {}, weak = {}",
			Rc::strong_count(&branch),
			Rc::weak_count(&branch),
		);

		println!(
			"leaf strong = {}, weak = {}",
			Rc::strong_count(&leaf),
			Rc::weak_count(&leaf),
		);
	}

	// As the leaf has no ownership relationship with the parent, there's no
	// problem with it going out of scope before the leaf itself. But from now on,
	// all the leaf will know about its parent is that is has left for some
	// cigarretes and became a `None`
	println!("leaf parent = {:?}", leaf.parent.borrow().upgrade());
	println!(
		"leaf strong = {}, weak = {}",
		Rc::strong_count(&leaf),
		Rc::weak_count(&leaf),
	);
}
