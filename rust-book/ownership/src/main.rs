#[allow(unused)]
fn main() {
    // Besides being a label for a value when immutable also represents a owner
    // of that value. This is the basis for the whole ownership on Rust, along
    // with the following rules:
    //  1. Each value must have a variable that's called its owner
    //  2. There can be only one owner at a time
    //  3. When the owner goes out of scope, the value will be dropped
    let name = "World";
    println!("Hello, {}!", name);

    let number = {
        // Since the variable `value` is within a block, which introduces a new
        // scope, it's only available here and once this scope finishes, the
        // value will be dropped from the stack
        let owner = 1;
        println!("The owner has {}", owner);

        // When a scope finishes, it can return a value since it's an expression
        // and thus we prevent the `owner` value from being dropped because it's
        // value may be used elsewhere by the new owner
        owner
    };

    println!("The number is {}", number);

    // The associated function `from` is used here to create a heap allocated
    // `String` from a stack allocated `str`. The type `String` allows us to
    // store an unknown amount of text because it is allocated on the heap
    // instead of the stack, much like dynamic memory allocation in C but done
    // automagically - with this type we can free ourselves from the immutable
    // fixed value strings.
    let mut greetings = String::from("Hello");

    // Despite being something allocated on the heap it's not mutable by
    // default, differently from many languages with GC based memory. Unless we
    // specify that `mut` modifier on greeting Rust would complain because
    // `push_str` needs to mutate the value - trackable mutability <3
    greetings.push_str(", world!");

    // And now you might be wondering what the hell is going to happen to our
    // heap allocated String once the main function ends. That is a reasonable
    // question since most languages only discard the stack allocated data,
    // which includes pointers to addresses of heap allocated data, and leave
    // the heap data to be cleaned by the garbage collector. Rust does this
    // automatically based on the ownership tracking of the value, if we don't
    // return it here, or lend somewhere else, then there's no new owner and
    // then the variable can be freed from memory automatically by Rust.
    // - Rust will automatically call the `drop` function which gives the
    //   creator of dynamic structures an opportunity for cleanup. This function
    //   is part of the Drop trait

    // Because these are basic primitive types, not pointers, we copy their data
    // from one owner to another and thus they share nothing and we're safe.
    let x = 5;
    let y = x;
    // However, with pointers it's a different story. Pointers are basically
    // ways to indicate where in memory something is placed, where that
    // something is dynamic and might be way bigger than a primitive type. Thus,
    // copying isn't an efficient procedure because we would duplicate something
    // with unknown size that might turn out problematic.

    // On the other hand, you might wonder why not just copying the pointer and
    // share the data. But this can lead to errors if you choose to free the
    // memory hold by all the pointers once they go out of scope, thus double
    // freeing the same piece of memory and crashing. To prevent such errors,
    // Rust instead considers `a` to be invalid and all further usage will result
    // on compilation error because rust **move** the pointer from one owner to
    // another. This is called a move operation and not simply a shallow copy
    // because the previous data is invalidated
    let a = String::from("a");
    let b = a;

    println!("Moved String is {}", b);

    // The variable `a` isn't valid anymore because it was moved to variable
    // `b`, hence it can't be used
    // println!("{}, world", a);

    // If you want something copied, you must say it! It's an expensive code
    // that copies every fucking thing so be careful, use it wisely
    let c = String::from("c");
    let d = c.clone();

    println!("c = {}; d = {}", c, d);

    // Now that you know that a move invalidates the previous owner, why does
    // the second declaration doesn't invalidate the variable `z`? That's due to
    // integers having a known size at compile time and being stored completely
    // on the stack, which speeds copying data. Beyond that, if you want to have
    // the same behaviour, any type that implements the `Copy` trait is capable
    // of that, however, to prevent the double free problem, any type that have
    // implemented `Drop` cannot implement `Copy` because it may lead to errors
    // depending on the cleanup code and Rust can give such freedom and trust
    // upon us without giving up some guaranties. Human err but Rust don't, just
    // by not trusting humans.
    // Types that implement `Copy`:
    // - integers
    // - booleans
    // - floating point types
    // - character type
    // - tuples with Copy only elements - (u32, f64) is and (i32, String)
    //   isn't because of String
    let z = 5;
    let k = z;

    println!("z = {}, k = {}", z, k);

    // When calling functions, the same mechanism takes place. When you call a
    // function with some data that can't be copied and need to be moved, you'll
    // eventually lose the ownership of that and then the owner becomes invalid
    // right after the function call. Because the function parameter now have
    // the ownership of the data and you don't, the value is dropped when that
    // scope ends and nothing is done here because the owner is invalid.
    let wallet = String::from("wallet");
    stealer(wallet);

    // For things that can be copied, everything lives on the stack and thus the
    // value is passed around through copies. In this case the value will be
    // dropped on both scope ends, here and on the function's scope because it
    // was copied.
    let balance = 10;
    bank(balance);
    println!("Fortunately, my money still remains with me, the bank just copied for its own use. I still have {}", balance);

    // Remember that a tuple can only be copied if all of its items are
    // copyable? Here before have to be moved to the parameter of `friend`,
    // however, if you analyse the code inside of that function you'll notice
    // that we didn't do a thing to `String` and used only the `i32`, and since
    // this is a `friend` it's supposed to return something to its owner (the
    // `main` scope). This is an example showing that returns also have the move
    // mechanics because after the function's scope terminates, the value isn't
    // dropped but instead moved to whomever is capturing the return of it.
    let amount = 10;
    let before = (String::from("Purse"), amount);
    let after = friend(before);

    // Here I'm intentionally using `amount` instead of `before.1` because
    // `before` isn't anymore valid once it was moved to `friend` parameter. We
    // could use the same name through shadowing for capturing the return value
    // of `friend` however I choose to leave it like this to show more clearly
    // how the mechanism works
    println!("What happened to {}? Now it's {} instead of {}", after.0, after.1, amount);


    let name = String::from("Bruno");

    // As returns also move the value to whomever is catching them, we can use
    // this to somewhat "lend" the value to a function and gather it back by
    // returning a tuple with the given variable. This way the caller can reuse
    // its variable after passing the parameter along with the result of
    // function's calculations. Which is exactly what happens on `length(x)`, it
    // uses the parameter to extract the length and return it inside the tuple
    // so the caller is able to rebind it to another (or the same, which is nice
    // due to shadowing) variable and work from there. Just remember that this
    // is only meaningful for variables that implement the `Drop` trait once
    // they can't be copied due to conflicts with the `Copy` trait
    let (name, length) = length(name);

    println!("The name '{}' has {} characters", name, length);

    let surname = String::from("Delfino");

    // Here we're just passing a reference of `surname` by prepending the
    // "reference operator" `&` to the variable name, this way we allow the
    // function to borrow this value without gaining ownership over. Removing
    // the need to write contrived tuples just to move it back to the caller,
    // returning the ownership to who had it, and effectively borrowing the
    // value.
    let len = len(&surname);

    println!("The length of '{}' is {}", surname, len);

    // To allow mutable references, we first need to have a mutable variable and
    // also pass it as a mutable reference. We have to be explicit all the way
    // down so Rust can track mutability and provide better guaranties
    let mut flattener = String::from("Is the world flat");
    with_question_mark(&mut flattener);

    println!("{}", flattener);

    // One nice restriction that Rust bestow upon us prevents the presence of
    // two simultaneous references with one of them being a mutable one. Such
    // restrinction guards us agains data races where someone points to a shared
    // data that supposedly is immutable while someone else, on the same scope,
    // is changing that data. The data race condition summarizes to the
    // coexistence of the following points:
    // - Two or more pointers access the same data at the same time.
    // - At least one of the pointers is being used to write to the data.
    // - There’s no mechanism being used to synchronize access to the data.
    // Since data races are difficult to track at runtime, Rust simply shields
    // us at compile time.

    let mut greeting = String::from("Hello, ");

    // That restriction can be "bypassed" with efemeral scopes using nested
    // blocks because as soon as the reference goes out of scope its pointer is
    // cleaned and there's no possibility to change a data that someone already
    // borrows as mutable or immutable. However, remember that this is only true
    // if this __mutable borrow__ happens before any __mutable borrow__ of the
    // upper scope, hence we can't try to bypass the restriction by using a
    // nested scope after the declaration of `karen`.
    {
        let chad = &mut greeting;
        chad.push_str("Chad");
    }

    // Almost the same applies for immutable references since they expect the
    // data to not be changed after they've referenced it, or even before. Thus,
    // you cannot have a mutable reference for the same data on the same scope
    // as a immutable reference, with the same trick and gotchas of bypassing
    // with blocks applied.
    // let hi = &greeting;

    // ~~However, since references point to data (memory blocks), Rust is able to
    // know that we are no longer sharing the same data after we reassign a
    // mutable variable and reference it afterwards, so the following is valid.
    // But as soon as you delete the reassignment, Rust will complain because
    // both the immutable and mutable references are pointing to the same place
    // in memory but with different expectations over changes and control.~~
    // While testing I had this feeling but it was totally wrong, I was even
    // thinking "Damn Rust, you're fucking smart!". I was simply not getting the
    // correct warning because I wasn't using the variable and it was discarded,
    // but as soon as I put the `println!` I got the same `cannot borrow as
    // mutable because it is also borrowed as immutable` warning, along with an
    // interesting warning telling that I cannot reassign `greeting` because it
    // was already borrowed
    // let hi = &greeting;
    // greeting = String::from("Hi");

    let karen = &mut greeting;
    karen.push_str("Karen");

    // Without the restriction of two simultaneous mutable references, the
    // variable `karen` which expects to have control over the `greeting` data
    // would have lost it as soon as `chad` comes into places and changes a
    // variable that she cares about, and within the same scope!
    // let chad = &mut greeting;
    // chad.push_str("Chad");

    // println!("The greeting before reassignment was: {}", hi);
    println!("Your greeting is: {}", karen);

    what_about_some_slices();
}

fn stealer(item: String) {
    println!("Haha it seems you've lost something. Could it be the ownership of {}?", item);
}

fn bank(money: i32) {
    println!("I've just copied your money to use somewhere else but it's still yours. See by yourself {}", money);
}

fn friend(item: (String, i32)) -> (String, i32) {
    println!("Hey, just used the {} and took 2 out of {}", item.0, item.1);
    (item.0, item.1 - 2)
}

fn length(text: String) -> (String, usize) {
    // Because of move, we can't simply do as below. Otherwise, Rust would need
    // to first move `text` to `.len` so it could calculate the length correctly
    // to only after moving it as the return of this method. It seems feasible,
    // but it's nothing so critical that couldn't be done with assigning the
    // value to variable before returning, and it removes a lot of complexity
    // from the compiler and borrow checker
    // (text, text.len())

    let length = text.len();

    (text, length)
}

// This function is a bit clever than `length` because it uses __referencing__
// to refer to a String without owning it. So the calculations can be done
// without moving the value from the caller and droping the value, the only
// thing that is dropped is the reference/pointer which in this case is called
// `text`, but the data which it points to remains intact and owned by the
// caller. References closely mirror pointers from C, in which they represent an
// "arrow" to some value stored somewhere else and you can read their value from
// there.
// References allow the caller to "lend" the value without worrying whether or
// not we will return the value so it can have the ownership back. We don't need
// to return the value to give back ownership because we never had it,
// references just *borrow* something from the caller.
fn len(text: &String) -> usize {
    text.len()
}

// References don't just allow us to borrow values without acquiring ownership
// over it, but they also permit borrowing mutable values so we can change them.
// This need to be used wisely so we don't end up with milions of mutations that
// difficult the reasoning of the value of a variable - 'GOTO was evil because
// we asked, "how did I get to this point of execution?". Mutability leaves us
// with, "how did I get to this state?"'.
// Despite the tracking of Rust from where the data is declared as mutable to
// where it's used as mutable, we need to be careful. Being as easy as adding
// `&mut` to a previously `&String` is very nice but play with care. Also,
// remember that, as of today, I'm a functional programmer by heart, so maybe
// I'll change my mind later.
fn with_question_mark(text: &mut String) {
    text.push_str("?");
}

// The following function won't compile unless we specify a lifetime for the
// returned reference, which is a concept I'm yet to learn. That happens because
// `s` is "bound" to the scope of this function and as soon as the function gets
// out of the stack, the owner will be cleaned and so its data. Thus, there
// can't be a reference pointing to "null" data because Rust prevents us from
// this hideous error.
// fn dangle() -> &String {
//     let s = String::from("data to be killed");

//     &s
// }

fn what_about_some_slices() {
    let text = String::from("I'm gonna be chopped");

    // The problem with this kind of pattern that returns indicators for some
    // portion of data instead of the portion itself (hopefully linked and
    // without copying to reduce memory footprint, because that's important for
    // languages that can be used on low memory devices) is that there's no link
    // between them and if we use those indices after the data is cleaned we'll
    // undoubtedly have an access problem
    let (_start, _end) = nth_word_indices(&text, 2).unwrap();

    // text.clear();
    // If we try to access the returned indices from `nth_word_indices` after
    // the string is cleaned we would have an error popping at our faces because
    // we've run into an ""IndexOutOfBoundsException"" since there's no data to
    // retrieve

    // The return of a reference allows us to prevent the somewhat related to
    // "use after free" error because of the reference rule where can't be a
    // mutable reference after a immutable was created. Because the method
    // `text.clear()` needs the self reference to be mutable, Rust is able to
    // detect that this would create simultaneous mutable and immutable
    // references.
    let word = nth_word(&text, 2).unwrap();

    // The following will fail because there can't be a mutable reference while
    // there's a immutable reference pointing to the same data
    // text.clear();

    println!("The second word is: {}", word);

    // If you have taken a peak over the implementation of `nth_word` you
    // probably saw that it uses `nth_word_indices`, and might be yelling at me
    // saying that it's all bullshit the "problem of use after free". Well, I
    // did delegated the search of the word to `nth_word_indices` to later
    // create the slice, but the real problem remains on the rules of references
    // regarding reference usage. The rule of thumb that string slices shows to
    // us is that if something is related to a portion of data, you should link
    // those as soon as possible to prevent any problem with "use after free".
    // Mainly due to the usage of a immutable reference can happen after the
    // real reference was changed, but the opposite isn't true because you can't
    // create a mutable reference while there a immutable reference to the same
    // data. And probably you're screaming at me again be realising that the
    // following would give the same results of broken compilation
    // ```
    // let mut text = String::from("I'm gonna be chopped");
    // let (start, end) = nth_word_indices(&text, 2).unwrap();

    // let word = &text[start..end];
    // // the error would still happen because this requires a
    // // mutable reference and we created the slice before it
    // text.clear();
    // println!("The second word is: {}", word);
    // ```

    // Now if you link that with my previous misconception of Rust being smart
    // as fuck to track reassignments of mutable data and ignore previous
    // immutable references because they would now point to a different data
    // from the following mutable. Basically making the following code valid.
    // ```
    // let mut greeting = String::from("hi");
    // let hi = &greeting;
    // // here I expected Rust to "unlink" the references
    // greeting = String::from("hello");
    // let hello = &mut greeting;
    // ```
    // And if you remember that the misconception was due to the no usage of the
    // immutable reference after the creation of a mutable reference, you'll
    // have the enlightment of knowing that the following is valid code.
    let mut text = String::from("I'm gonna be chopped");
    let (start, end) = nth_word_indices(&text, 2).unwrap();

    let word = &text[start..end];
    println!("The second word is: {}", word);

    text.clear();
    println!("Is the text empty? {}", text.is_empty());

    // But as soon as I use the immutable reference after the creation of a
    // mutable one, I'll get an error saying that they can't exist
    // simultaneously. This happens only if you choose to use it after the
    // mutable because Rust considers a references scope to be from where it was
    // introduced until the last place where it was used.
    // println!("The second word is: {}", word);

    // The usage of str reference on the signature is more broad and allows str
    // and String
    let text = "String Literal";
    let (start, end) = nth_word_indices(text, 2).unwrap();

    let word = &text[start..end];
    println!("The second word is: {}", word);
}

// With the knowledge of String Slices we are able to understand that *String
// Literals Are Slices* of the binary. String literals are basically pointers to
// a portion of the binary with their length being the end of the slice, and for
// sure there's some wizardry around that than just "slices of the binary".
// Since string literals have the type `&str` - the same as String Slices - and
// that we can create slices out of `String`, we can take advantage of that and
// create a more generic API by accepting slices instead of just `&String`, so a
// literal can be passed as `nth_word_indices("string literal", 1)` and a heap
// allocated string with `nth_word_indices(&text[..], 1)` - the usage of a
// `RangeFull` captures the whole String.
// PS: The creation of a slice for the full String seems to be now unnecessary,
// the code didn't break with the change. Probably an improvement on the latest
// versions since the Rust Book was released.
fn nth_word_indices(text: &str, index: usize) -> Option<(usize, usize)> {
    let mut start = 0;
    let mut end = text.len();
    let mut countdown = index;
    let mut was_space = true;

    for (i, &byte) in text.as_bytes().iter().enumerate() {
        if byte != b' ' && was_space {
            start = i;
            countdown -= 1;
        }

        was_space = byte == b' ';
        if countdown == 0 && was_space {
            end = i;
            break;
        }
    }

    if countdown != 0 {
        None
    } else {
        Some((start, end))
    }
}

// The below comment is a documentation comment. It's a comment that starts with
// `///` instead of `//` and that supports markdown. Along with that,
// documentation comments are read when publishing the crate and end up on HTML
// documentation that can be checked by running `cargo doc --open`.
// These special comments can have a title, the first line, which shows on
// listings, and a more detailed description containing headers and even code
// blocks that can be used as doc tests - although doc tests only run for lib.
// The commonly used sections on documentation comments are Examples, Panics,
// Error, and Safety. Which are about, respectively: usage; scenarios that
// panic; causes, types, and mitigations for the Err branch on Result; why the
// function is unsafe and invariants that the caller should uphold prior to
// calling the function

/// Returns the nth word based on whitespace characters
///
/// # Examples
///
/// ```
/// let phrase = "I don't know what I'm doing";
/// let word = nth_word(phrase, 5);
///
/// assert_eq!(word, "I'm");
/// ```
pub fn nth_word(text: &str, index: usize) -> Option<&str> {
    // This notation of `&text[s..e]` denotes a String slice, with is a
    // reference with additional data to get just a portion of that with the
    // help of a Range from `s` to `e - 1` (for inclusive end you should use
    // `s..=e`). With this special type of reference we're able to get only a
    // portion of the string in `text` with a link between them.
    nth_word_indices(text, index).map(|(s, e)| &text[s..e])
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn first_word() {
        assert_eq!(Some("rust"), nth_word("rust book", 1));
        assert_eq!(Some("tests"), nth_word("tests book", 1));
    }

    // #[test]
    // fn zeroth_word() {
    //     assert_eq!(None, nth_word("some text", 0));
    //     assert_eq!(None, nth_word("another text", 0));
    // }

    #[test]
    fn out_of_bounds_word() {
        assert_eq!(None, nth_word("strange behavior happens", 4));
        assert_eq!(None, nth_word("strange behavior happens", 7));
    }

    #[test]
    fn no_words_string() {
        assert_eq!(None, nth_word("", 1));
        assert_eq!(None, nth_word("    ", 2));
    }

    #[test]
    fn padded_string() {
        assert_eq!(Some("left"), nth_word("   left pad", 1));
        assert_eq!(Some("pad"), nth_word("big right pad       ", 3));
        assert_eq!(Some("both"), nth_word("   pad both sides   ", 2));
    }
}
