// Enums help us express enumerable things of which we can write down all the
// possible variants, also we must note that those variants are completely
// separated from each other and a variable can't be more than one at same time.
// The declaration of an enum consists of the keywod `enum` followed by its name
// `IpAddrKind` for this case, and a block with the possible values for that
// enum. Enums are very similar to algebraic data types from Haskell where the
// enum's name is the type and the values/variants are the instances for that
// value
#[derive(Debug)]
enum IpAddrKind {
    // An enum without data might come in handy sometimes where you don't need
    // to associate it with something that is intrinsic to its type and might be
    // considered invalid if not accompanied by such data. You might handle this
    // with separated fields within a struct where one is the struct for the Ip
    // kind and another is the actual address, but there's no need for that
    // since we can put data directly in the struct by using the tuple struct
    // notation
    V4(u8, u8, u8, u8),
    // Just like Haskell again, a variant of a struct (a value constructor) can
    // receive and arbitrary amount of parameters and they don't need to conform
    // to a single structure like Java Enums. Also, this is not limited to
    // primitive data types, the stdlib itself has the representation of IPs and
    // uses two separate structs `Ipv4Addr` and `Ipv6Addr` for the correspondent
    // variant. And differently from the previous variant, this use a normal
    // struct notation instead of a tuple struct; and for these kind of
    // variants, they are said to hold an anonymous struct inside of them
    V6 { addr: String }
}

// Jus like ~~Haskell~~ structs, enums can have implementations thus having a
// common protocol between all variants, this ressembles the enum classes from
// Java. Also, they can implement derivable traits as you can see when printing
// the value
impl IpAddrKind {
    // Like I said, we use the enum name as the type and not the instances just like
    // in Haskell
    fn route(&self) {
        // This is the match control flow operator, it allows us to match (duh?)
        // a value against a set of patterns and then execute the code for the
        // first match. Patterns can be many things:
        //  - variable names as capture all;
        //  - literal values to check for specific values;
        //  - underscore to capture all but ignore the value;
        //  - multiple patterns with `|`;
        //  - range matches with `1..5`;
        //  - destructuring patterns that match the structure;
        // Match structures receive a value that will go through each of the
        // patterns listed within its body, in the format of `pattern => { body
        // }, ...` - with the brackets/block being optional if you use a single
        // expression - and the execution will follow the first match, much like
        // a coin sorter where you specify the more specific first (smaller
        // holes) and later the more broad cases (larger holes). They are
        // similar switch-case statements from some languages but are much more
        // powerfull given the usage of pattern matching, the only thing that we
        // need to remember is that each arm must return the same type so the
        // compiler can infer the correct type for the given match expression as
        // a whole
        let rep = match self {
            // This is an example of simultaneously matching `self` against
            // `IpAddrKind::V4` to check wether this is an instance of it, and
            // destructuring the tuple containing the values to capture each
            // component in a specific variable
            IpAddrKind::V4(first, second, third, fourth) =>
                format!("{}.{}.{}.{}", first, second, third, fourth) ,
            // The only difference here is that we are matching against a
            // different variation and destructuring a struct instead of a
            // tuple.
            // There're two ways to destruct a struct to extract the
            // values from the fields and assign them to variables, we can
            // choose to use the same name as the field by only using the field
            // name, e.g., `{ addr }` would create a variable `addr` that
            // contains the value from the `addr` field - ressembling what JS
            // have. The other way to extract the values is to assign it to a
            // variable with a different name by using `{ addr: address }` which
            // creates a variable called `address` containing the value of
            // `addr` - again, that resembles the syntax from JS
            IpAddrKind::V6 { addr: address } => String::from(address),
        };

        println!("You're pointing to {}", rep)
    }
}

fn main() {
    // We instantiate a variant from the `IpAddrKind` by using the double colon
    // because they are associated with the `IpAddrKind` type
    let four = IpAddrKind::V4(127, 0, 0, 1);
    let six = IpAddrKind::V6 { addr: String::from("::1") };

    four.route();
    six.route();

    // The `Option` enum helps us encode the idea of something that might not be
    // there due to some circunstances. It encodes the idea of `null` from many
    // languages in a way that the compiler nows ahead of time that something
    // might be null instead of leaving everything to be null and crash on
    // runtime
    let seven = Some(7i32);
    // Given that the `None` variation from `Option` doesn't have the field to
    // relate it with the type parameter from `Option<T>` as in `Some(T)` we
    // have to explicitly tell the parameter for the Option type, to create the
    // `Option<i32>` from the `Option<T>` "constructor" - higher kinded types
    let five: Option<i32> = None;

    println!("Result of adding 3 to {:?} is: {:?}", five, add(five, 3));
    println!("Result of adding 3 to {:?} is: {:?}", seven, add(seven, 3));

    fizzbuzz(33);
}

fn add(term: Option<i32>, addend: i32) -> Option<i32> {
    // Match expressions have to be exhaustive, they need to either list all the
    // variants for the given type or a catch-all for the last pattern,
    // otherwise the compiler will complain since this code might turn out
    // eroneous for some scenarios. Here we've choosen to list None explicitly
    // but we could have used the `_` on the last pattern without a problem
    match term {
        Some(val) => Some(val + addend),
        None => None,
    }
}

fn fizzbuzz(limit: u8) {
    for n in 1..=limit {
        // In this match expression we are being exhaustive by ignoring the
        // pieces that we don't care about for specific cases and ignoring
        // everything at the end.
        let message = match (n % 3, n % 5) {
            (0, 0) => String::from("FizzBuzz"),
            (0, _) => String::from("Fizz"),
            (_, 0) => String::from("Buzz"),
            _ => format!("{}", n)
        };

        println!("{}", message)
    }
}

fn print(sub: Option<i32>) {
    // Another option to the match, sometimes useful when you want to just check
    // a value, is the `if let` combination. With this you can write a refutable
    // pattern, a pattern that might fail, which is otherwise invalid for simple
    // let declarations since it may fail and break everything. This code will
    // only execute if the pattern match, otherwise it'll follow the control
    // flow of other `else if` or `else`; also, note that you can write `if
    // let`, `else if let` and so on, but this probably will be more verbose
    // than match expressions.
    // One downside of this is that the compiler won't tell you to be exhaustive
    // when matching, it'll consider that you known that you have a refutable
    // pattern at hand and sometimes it might fail. But this doesn't mean that
    // you will have an error, because if the pattern fails the variables that
    // bound when destructuring will never exist anyway.
    if let Some(val) = sub {
        println!("The value is {}", val)
    // This else act just like the `_` pattern to catch everything
    } else {
        println!("You probably have nothing in your hands")
    }
}
