// This is the most basic declaration of a struct, it has a name and a set of
// fields. It's like a tuple when it comes to grouping data but with structs you
// rely on fields instead of positions
struct User {
    // You might wonder, why not using references so the struct doesn't own this
    // data? This is done so all the data within the struct is valid during the
    // whole life of the struct, to have structs with references we need
    // lifetimes and I don't have a clue of what that is by the time being.
    username: String,
    email: String,
    sign_in_count: u64,
    active: bool
}

impl User {
    // This is a common pattern in Rust to indicate a type constructor. Note
    // that it's not standardized by the language, it's more like a gentlemen
    // agreement on constructor names on Rust.
    fn new(username: String, email: String) -> Self {
        Self {
            // Just like JS, we can use a feature called __field init shorthand__ to
            // assign concrete values to a field when the field and variable share
            // the same name
            username,
            email,
            sign_in_count: 1,
            active: true
        }
    }
}

// We can also have Unit-Like structs which have no fields and might come in
// handy on some situations. They are called Unit-Like due to behaving
// similarly to `()`
struct Unit;

// Some traits can be automatically implemented by using these annotations of
// #[something(params)]. To have a trait automatically implemented we tell Rust
// to derive it for us, so the annotation is as follows. There are other traits
// that Rust provides with we can derive and those are called __Derivable
// Traits__
#[derive(Debug)]
struct Rectangle {
    width: usize,
    height: usize
}

// This starts the implementation context/block for Rectangle which will contain
// methods and associated functions of Rectangle. You can have as many of these
// as you want, you can even separate an impl block for each of your methods or
// associated functions.
impl Rectangle {
    // Methods are just like functions but they are associated to the
    // implementation context of a struct (or enum or trait). The first
    // parameter is always `&self`, much like Python, so they can act on the
    // object itself without owning it. We don't need to use `rect: Rectangle`
    // as parameter because Rust knows what type self has and self is somewhat
    // like an keyword for Rust to allow this to be called on instances of
    // Rectangle. Just one nice thing to notice is that the reference isn't
    // actually mandatory to consider it a method, we can still choose to gain
    // ownership over the instance, we didn't because we don't need, but self is
    // like any other parameter, we can declared mutability and references at
    // will - according to the book, methods that take ownership are rare and
    // used when you want to prevent the caller from using the type after the
    // method, might be wanted on some sorts of transformations
    fn area(&self) -> usize {
        self.width * self.height
    }

    // Here you can see a nice type which can increase the flexibility of our
    // methods when we want to rename the type. The type Self is the type of the
    // current implementation, it's also used on `self` which hopefully Rust did
    // the homework and made `self` a syntax sugar for `self: Self`
    fn can_hold(&self, rect: &Self) -> bool {
        let normal = (self.height >= rect.height, self.width >= rect.width);
        let flipped = (self.height >= rect.width, self.width >= rect.height);

        match (normal, flipped) {
            ((true, true), _) => true,
            (_, (true, true)) => true,
            _ => false
        }
    }

    // This is called an associated function because it's associated to the
    // struct itself and not to an intance of the struct since it doesn't take
    // `self` as the first parameter. These function are mostly used as
    // constructors like `String::from` and the common pattern of `Name::new`
    fn square(size: usize) -> Self {
        Self { width: size, height: size }
    }
}

fn main() {
    // To instantiate a struct you must pass concrete values to each of the
    // fields. You start with the name of the struct, then a block containing
    // each `key: value` pair separated by comma - you can make this a one liner
    // but be reasonable when to split up.
    let john = User::new(
        String::from("john"),
        String::from("john@example.com")
    );

    // To access a field you use the common dot notation of `variable.field`
    println!("Hello, {}!", john.username);

    let mut doe = User::new(
        String::from("doe"),
        // Rust accept trailing commas for function calls and arrays \o/
        String::from("doe@example.com"),
    );

    // If you want to change a field of a struct you must declare the struct as
    // mutable, quite differently from most languages of which you get only
    // immutability for the root/reference and not for the components within it.
    // Rust is the opposite from JS and Java where you can declare a variable
    // immutable but still change its properties
    doe.active = true;

    let doe = sign_in(doe);
    print_user(&doe);
    println!(
        "has signed in {} time{}",
        doe.sign_in_count,
        if doe.sign_in_count == 1 { "" } else { "s" }
    );

    // When giving names to each field sounds a little bit too verbose, you can
    // use tuple structs and let the use guess what each one represents. It's
    // handy for declaring small structures where the name almost infers the
    // order once you know the concept, like the following Color where fields
    // are values for the RGB.
    struct Color(u8, u8, u8);
    // Despite representing the same tuple in terms of size and types, they are
    // different types because the names are different and Rust won't integrate
    // those on a single tuple
    struct Point(u8, u8, u8);

    let red = Color(255, 0, 0);
    println!("The color components are: red({}), green({}), blue({})", red.0, red.1, red.2);

    let origin = Point(0, 0, 0);
    println!("You are at: x({}), y({}), z({})", origin.0, origin.1, origin.0);

    let square = Rectangle::square(5);

    // This is how you call a method on Rust, and differently from C and C++ you
    // don't have to dereference the pointer before calling the method by using
    // either some C like notations as `square->area()` or `(*square).area()`.
    // Rust have a feature called __automatic referencing and dereferencing__
    // which is used when calling methods
    let area = square.area();

    // The specifier {?:} tells println! to use the Debug output format, which
    // is different from the usual Display format. Both of these are traits that
    // can be implemented to return the wanted formating.
    // The `Display` trait is implemented for all the primitive types because
    // there's no ambiguity when displaying them, there's only one way to show a
    // `1u32` or a `true`.
    // For `Debug` we have the usual `{:?}` and the pretty print `{:#?}`.
    println!("The area of ({:#?}) is {}", square, area);

    let rect = Rectangle { width: 10, height: 3 };
    let inverted_rect = Rectangle { height: 10, width: 3 };
    println!("Can rect ({:?}) hold square ({:?})? {}", rect, square, rect.can_hold(&square));
    println!("Can rect ({:?}) hold inverted rect ({:?})? {}", rect, inverted_rect, rect.can_hold(&inverted_rect));
}

fn sign_in(user: User) -> User {
    User {
        sign_in_count: user.sign_in_count + 1,
        // Again a similarity wit JS, this is called __struct update syntax__ and
        // allows us to copy all the values from `user` but the ones we've declared
        // before. However, differently from JS the order is inverted because we
        // read it like "get the remaining fields from `user`" and not as JS where
        // we read it "get all fields from `user` and then overwrite the following"
        ..user
    }
}

fn print_user(user: &User) -> Unit {
    println!("username: {}, email: {}", user.username, user.email);
    Unit
}
