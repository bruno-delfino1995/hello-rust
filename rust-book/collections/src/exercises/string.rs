// This considers only ASCII strings, I'm not in the mood to understand which
// letters are consonants and which aren't for non english alphabets
pub fn main() {
    let lorem = "Lorem ipsum dolor sit amet consectetur adipiscing elit";

    for word in lorem.split_ascii_whitespace() {
        print!("{} ", to_pig_latin(word))
    }
    println!("")
}

fn to_pig_latin(word: &str) -> String {
    let first_letter = get_first(word);
    if !is_vowel(first_letter) {
        let remaining = tail(word);

        format!("{}-{}ay", remaining, first_letter)
    } else {
        format!("{}-hay", word)
    }
}

fn get_first(word: &str) -> char {
    word.chars().nth(0).unwrap()
}

fn is_vowel(letter: char) -> bool {
    const VOWELS: [char; 10] = ['a', 'e', 'i', 'o', 'u', 'A', 'E', 'I', 'O', 'U'];

    VOWELS.contains(&letter)
}

fn tail(word: &str) -> String {
    let mut iter = word.chars();

    // Discard first
    iter.next();

    iter.as_str().to_string()
}
