use rand::Rng;

use std::ops::Range;
use std::ops::RangeBounds;
use std::ops::Bound;

use std::collections::HashMap;

pub fn main() {
    let numbers = gen_numbers(1..10, 30);
    println!("The numbers are: {:?}", numbers);

    println!(
        "Here are the statistics about numbers: mean({:.2}), median({}), mode({})",
        mean(&numbers),
        median(&numbers),
        mode(&numbers)
    );
}

fn gen_numbers(range: Range<isize>, amount: u8) -> Vec<isize> {
    let start = match range.start_bound() {
        Bound::Included(v) => *v,
        _ => 0,
    };

    let end = match range.end_bound() {
        Bound::Included(v) => v + 1,
        Bound::Excluded(v) => *v,
        _ => 0,
    };

    let mut numbers = vec![];

    for _ in 1..=amount {
        let number = rand::thread_rng().gen_range(start, end);
        numbers.push(number);
    }

    numbers
}

fn mean(numbers: &Vec<isize>) -> f32 {
    let total: isize = numbers.iter().sum();

    total as f32 / numbers.len() as f32
}

fn median(numbers: &Vec<isize>) -> isize {
    let mut copy = numbers.to_owned();
    copy.sort();

    *copy.get(numbers.len() / 2).unwrap()
}

fn mode(numbers: &Vec<isize>) -> isize {
    let mut amounts = HashMap::new();

    for v in numbers {
        let amount = amounts.entry(v).or_insert(0);
        *amount = *amount + 1
    }


    amounts
        .iter()
        .max_by(|(_, a), (_, b)| a.cmp(b))
        .map(|k| **k.0)
        .unwrap()
}
