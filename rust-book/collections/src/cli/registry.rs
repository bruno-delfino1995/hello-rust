use std::collections::HashMap;

pub struct Registry {
    contents: HashMap<String, Vec<String>>,
}

impl Registry {
    pub fn new() -> Self {
        Registry {
            contents: HashMap::new(),
        }
    }

    pub fn add(&mut self, (name, dep): (&String, &String)) {
        let val = self.contents.entry(dep.to_string()).or_insert(vec![]);
        val.push(name.to_string());
    }

    pub fn del(&mut self, (name, dep): (&String, &String)) {
        let val = self.contents.entry(dep.to_string()).or_insert(vec![]);

        val.retain(|v| v != name)
    }

    pub fn list(&mut self, dep: &String) -> Vec<String> {
        let mut vec = self
            .contents
            .entry(dep.to_string())
            .or_insert(vec![])
            .to_vec();

        vec.sort();

        vec
    }
}
