mod command;
mod registry;

// Write is for flushing when using `print!` without a subsequent `println!`
use std::io::{self, Write};

use crate::command::Command;
use crate::registry::Registry;

fn main() {
    let mut registry = Registry::new();

    print!("Hello to the department manager 3000!!");
    loop {
        let mut command = String::new();

        prompt(&mut command);

        let result = match Command::new(command) {
            Some(command) => execute(command, &mut registry),
            _ => Ok(String::from("Unknown command, please try again")),
        };

        match result {
            Ok(message) => println!("{}", message),
            Err(error) => {
                println!("{}", error);
                break;
            }
        }
    }
}

fn prompt(command: &mut String) {
    println!(
        r#"
Commands:
  - Add {{}} to {{}}
  - Remove {{}} from {{}}
  - List {{}}
  - Quit"#
    );

    print!("What you want to do? ");
    io::stdout().flush().expect("unable to flush stdout!!!");

    io::stdin()
        .read_line(command)
        .expect("failed to read command");
}

fn execute(command: Command, registry: &mut Registry) -> Result<String, String> {
    match command {
        Command::Add { name, department } => {
            registry.add((&name, &department));

            Ok(format!("{} was successfully added to {}", name, department))
        }
        Command::Remove { name, department } => {
            registry.del((&name, &department));

            Ok(format!(
                "{} was successfully removed from {}",
                name, department
            ))
        }
        Command::List { department } => {
            let people = registry.list(&department);
            let mut prompt = format!("In the {} deparment are:", department);

            for p in people {
                prompt.push_str(&format!("\n  - {}", p));
            }

            Ok(prompt)
        }
        Command::Quit => {
            // TODO: this isn't really an error, I should find a better enum
            Err(String::from("Bye bye!!"))
        }
    }
}
