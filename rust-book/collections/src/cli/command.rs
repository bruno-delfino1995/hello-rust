use regex::Regex;

pub enum Command {
    Add { name: String, department: String },
    Remove { name: String, department: String },
    List { department: String },
    Quit,
}

impl Command {
    pub fn new(command: String) -> Option<Self> {
        let re = Regex::new(r"(?i)quit").unwrap();
        if re.is_match(&command) {
            return Some(Self::Quit);
        }

        let re = Regex::new(r"(?i)add (\w) to (\w)").unwrap();
        if re.is_match(&command) {
            let captures = re.captures(&command).unwrap();

            return Some(Self::Add {
                name: String::from(&captures[1]),
                department: String::from(&captures[2]),
            });
        }

        let re = Regex::new(r"(?i)remove (\w) from (\w)").unwrap();
        if re.is_match(&command) {
            let captures = re.captures(&command).unwrap();

            return Some(Self::Remove {
                name: String::from(&captures[1]),
                department: String::from(&captures[2]),
            });
        }

        let re = Regex::new(r"(?i)list (\w)").unwrap();
        if re.is_match(&command) {
            let captures = re.captures(&command).unwrap();

            return Some(Self::List {
                department: String::from(&captures[1]),
            });
        }

        None
    }
}
