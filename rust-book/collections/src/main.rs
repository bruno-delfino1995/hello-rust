mod exercises;
mod hash_map;
mod string;
mod vec;

fn main() {
    println!("Understanding Vectors\n");
    vec::main();

    println!("\nUnderstanding Strings\n");
    string::main();

    println!("\nUnderstanding HashMaps\n");
    hash_map::main();

    println!("\nPracticing a little\n");
    exercises::main();
}
