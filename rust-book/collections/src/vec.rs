pub fn main() {
    // One of the basic data structures categorizes as collection is the `Vec`,
    // or sometimes known as `List`, which is a dinamically allocated array of
    // elements that sit next to each other in memory. Vec is a way to group
    // elements within an array of unknown size at compile time.
    // Below we are declaring a Vec with its generic parameter bound to `i32`,
    // which means it's going to accept only items with the i32 type. We don't
    // really need to annotate the type if we push elements into the Vec, but as
    // this is for learning purposes, we made it clear here
    let v: Vec<i32> = Vec::new();

    // This is another way to declare a Vec which uses the `vec!` macro to help
    // deriving a Vec from an array. Remember that since we're using integers,
    // the default type is `i32`
    let ve = vec![1, 2, 3];

    println!("Both vectors are {:?} and {:?}", v, ve);

    let mut vec = vec![];
    // This pushes an element to the end of the vector, and from this Rust is
    // able to infer the type
    vec.push(1);
    vec.push(2);

    println!("The contents of vec is {:?}", vec);

    // Given that Vec is a dynamic data, all the data within it will be cleaned
    // once the owner gets out of scope. However, if it holds references to
    // data, only those references will be dropped while the data remains with
    // its original owner, following what you probably known after understanding
    // ownership
    let mut vecr = vec![];
    let data = String::from("something");
    vecr.push(&data);

    // I'm using strings here to be sure that when playing with owership down
    // below the data is actually being moved and not copied
    let mut vect = vec![
        String::from("a"),
        String::from("b"),
        String::from("c"),
        String::from("d")
    ];

    // To read data from Vec you can use the common array notation with an
    // downside of insecurity against "IndexOutOfBoundExceptions", meaning that
    // the code will panic if we try to get from an index that doesn't exist.
    // Another interesting characteristic is that we cannot acquire ownership
    // over one specific item, for that reason we need to use references when
    // trying to read from the vector. If you were using a Vec<i32> you could do
    // this because `i32` implements `Copy` so what actually happens is a copy
    // and not a move, but since the `vect` is a `Vec<String>` the following
    // would result in an error because you cannot move an index out of vec
    // ```
    // let el = vect[2];
    // println!("{:?}: {}", vect, el); // ["a", "b", "c", "d"]: "b"
    // ```
    let el = &vect[2];
    println!("The element at index {} from {:?} is {}", 2, vect, el);
    vect[2] = String::from("e");
    vect.push(String::from("f"));

    // Independently on whether you're using a reference or not, if the index is
    // out of bounds for the given vector, you'll get a runtime error because
    // your program will panic. For a more secure version, Vec has the `get`
    // method that returns a `Option<T>`
    if let Some(el) = vect.get(2) {
        println!("The element at index {} from {:?} is {}", 2, vect, el)
    }

    // Also, respecting the references rules under ownership and borrowing, we
    // cannot have a mutable reference while a immutable reference exists, so
    // the following is invalid
    // ```
    // let vect = vec![1, 2, 3];
    // let el = &vect[0];
    // v.push(6); // Here push needs to have an immutable reference to &self
    // println!("{}", el); // As the immutable reference lives after the mutable, the problem happens.
    // ```
    // This is particularly important for vectors because they are contiguous in
    // memory, which means that all items are allocated as neighbours. This is a
    // huge pro for access performance, however, when you try to push an
    // element, the whole array have to be reallocated if there isn't enough
    // space to fit the new. The reallocation of the whole array is particularly
    // problematic when we are dealing with references because pushing an item
    // have a high probability of making all the previous references point to
    // invalid memory addresses

    let mut vecto = vec![10, 20, 30];

    // We can use Vec as a normal iterator if we were to run through all of its
    // elements
    for el in &mut vecto {
        // Just as in C, if you want to change a value of a reference, you must
        // go to the referenced place first by using the __deref operator__ `*`.
        *el += 10;
    }

    // How do you overcome the inate need of dynamic programmers for
    // heterogeneous data structures? Just group the structures under a common
    // name and voila!
    // This is exactly how you can hold more than one type into the same vector
    // with static type checking and guarantees when reading such values, just
    // use an enum to group all the desired values under it and use the enum as
    // the generic type parameter.
    enum Values {
        // Enum variants are types on their own, we can't just use `i32` or
        // `String`, because firstly the variants need to be PascalCase and
        // secondly the String is going to be `Values.String` and not just the
        // raw `String`. That's why we need to wrap them in variants that look
        // like "tuple structs"
        Int(i32),
        // We could have called this variant String without a problem because
        // the name is under Values and won't clash with the usual String
        Text(String),
        Bool(bool)
    }

    let vector = vec![Values::Int(1), Values::Text(String::from("Xablau")), Values::Bool(false)];

    if let Some(Values::Bool(val)) = vector.get(1) {
        println!("This should never be printed because val({}) at index 1 is not a bool", val);
    }

    if let Some(Values::Text(val)) = vector.get(1) {
        println!("Haha, this pattern matched and the element is: {}", val)
    }

    // Rust is extremely rigorous when pattern matching and makes sure that we
    // match all the variants on `Some` so there can't be runtime errors
    let kind = match vector.get(2) {
        Some(Values::Int(_)) => "integer",
        Some(Values::Bool(_)) => "bool",
        Some(Values::Text(_)) => "string",
        // At this position this one is redundant and unnecessary, but this is a
        // way to match all and don't care to the value itself
        // Some(_) => "value",
        None => "unknown",
    };

    println!("The type of the element at index {} is {}", 2, kind);
}
