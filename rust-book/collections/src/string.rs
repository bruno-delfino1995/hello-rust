use unicode_segmentation::UnicodeSegmentation;

// When Rust refers to strings it's refering at both the string slice, in the
// language core as primitive type `str`, and the String, which is a growable
// mutable owned. Both string types are references to UTF-8 encoded somewhere.
// Also, there are some other string types provided by Rust such as `OsString`,
// `OsStr`, `CString`, and `CStr`; they refer to owned and borrowed variants
// String and str.
#[allow(unused)]
pub fn main() {
    // To create an empty string you can start with the following. But this
    // isn't so common because normally you'd start a string from something
    // else.
    let mut s = String::new();

    // We can to choose between the method `to_string` which any type that
    // implements `Display` have, or we can go with `String::from` to create it
    // from a string literal
    s = "concents".to_string();

    // As strings reference UTF-8 data, we can create a growable string from a
    // string literal with UTF-8 characters
    s = String::from("السلام عليكم");

    println!("The string is: {}", s);

    // To append content to a string, you can choose between the `push_str`
    // which receives a string slice, `push` that receives a character, or the
    // convenient `+` operator
    let concat = " <- I don't know what this means ";
    // Since `push_str` uses a reference to a string slice, we can use the
    // appended variable after appending it without any problem
    s.push_str(concat);

    // Characters are also UTF-8, just as strings!!
    s.push('😱');

    println!("{}", s);

    let s1 = String::from("Hello");
    let s2 = String::from(", world!");

    // The reason on why we need to acquire ownership of the first and just
    // borrow the second, goes down to how the `+` is implemented for the
    // str/String (I don't know who implements this) type.
    // To be something that can be added, Rust types provide implementations of
    // the `Add` type, thus the type is now have the operator `+` enabled for
    // it. Behind the scenes, Rust will convert things like `x + y` to
    // `x.add(y)`, so in the end we have to adhere to the signature of `add`.
    // Which for strings goes something like `fn add(self, s: &str) -> String`,
    // and that is why we move `s1` but just reference `s2`. One interesting
    // characteristic worth of highlighting here is that Rust has a feature
    // called __deref coercion__, which here is able to turn a &String into a
    // &str. Also, it's worth noting that the implementation is smart enough to
    // not copy things around and rather it just gets the ownership to return it
    // after
    let s3 = s1 + &s2;

    println!("I wish we could print s1, but it was moved, so here goes everything: {}", s3);

    let tic = String::from("tic");
    let tac = String::from("tac");
    let toe = String::from("toe");

    // As `add` returns the ownership and so its a valid `str` we can chain
    // multiple `+` operations with no problem, and for the literals Rust has no
    // problem with them because they are already `&str`
    let game = tic + "-" + &tac + "-" + &toe;

    // However, multiple `+` might be troublesome and you can use the might
    // format!. NOTE: I'm using the literal "tic" because the variable was moved
    let game = format!("{}-{}-{}", "tic", tac, toe);

    // Unfortunately, Rust strings don't accept accesses for specific indexes.
    // That's because strings are seen as a set of UTF-8 characters, so when a
    // programmers wants to access the element at index 0 they normally want the
    // character at index 0. Since there could be a misaligment between the
    // programmer's expectation and what needs to be done to correctly support
    // UTF-8 strings, they've choosen to not support indexing at all.
    // That's because the String type is a "proxy" to the a Vec<u8> which stores
    // the bytes needed for each character, and sometimes one character takes
    // more than one single byte, so when you mean `&"something"[0]` you're
    // actually going for the first byte and not the first character.
    // For simple ASCII strings there's no problem, but when you accept
    // something like "😱" a valid character, the first byte won't be the full
    // character. And if you want to access the character anyway, there couldn't
    // be a way to have the expectation of O(1) time when accessing with indexes
    // met because Rust would need to inspect the surrounding bytes to determine
    // the character

    let hindi = String::from("नमस्ते");

    // When talking about strings, Rust has the following three ways to
    // represent them: (1) bytes, which match to the integers stored in memory;
    // (2) chars, which are what you type and with each composition component
    // separated, like when you type "ç" on non brazilian keyboard by composing
    // two keys, the letter and a diacritic; (3) graphemes, which are more
    // close to what we think as letters.
    let bytes = hindi.as_bytes();
    let chars = hindi.chars();
    let graphemes = UnicodeSegmentation::graphemes("नमस्ते", true);

    // However, if you remember from previous chapters, we can take out string
    // slices from String by using &var[s..e]. But one thing we didn't say is
    // that we can have slices only for full characters, we cannot slice only
    // half bytes of a character, otherwise Rust will panic at runtime. Thus, be
    // carefull when using string slices
    let first_diacritic = &hindi[9..12];
    println!("This a diacritic: {}", first_diacritic);

    for (i, ch) in hindi.chars().enumerate() {
        println!("char at {} is {}", i, ch);
    }

    for (i, b) in hindi.bytes().enumerate() {
        println!("byte at {} is {}", i, b);
    }
}
