mod vec;
mod string;

pub fn main() {
    println!("It's time for some vectors");
    vec::main();

    println!("\nWhat about some strings?");
    string::main();
}
