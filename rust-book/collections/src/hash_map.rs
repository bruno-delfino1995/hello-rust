// HashMaps are stores that associate a value to a key which are then placed in
// memory according to a hashing function. As they aren't so often used like Vec
// and String, you need to import them
use std::collections::HashMap;

pub fn main() {
    // Like any data structure with a single way of creating, just use ::new.
    // Also, as we're going to insert keys on it, it needs to be mut
    let mut numbers: HashMap<String, u8> = HashMap::new();

    // You probably guessed what this does
    numbers.insert(String::from("one"), 1);
    numbers.insert(String::from("two"), 2);

    let teams = vec!["bravo", "charles"];
    let scores = vec![10, 15];

    // We can create hashmaps from vectors of pairs with key and value and use
    // `collect` on that vector to convert the type. Here we need to annotate
    // the type with HashMap because collect can convert to many data types,
    // however, we can ignore the type parameters because Rust will be able to
    // infer those from the collections
    let teams_scores: HashMap<_, _> = teams.into_iter().zip(scores.into_iter()).collect();

    // When using hashmaps we can use anything as key or value, even references,
    // however, the references need to be valid for at least the lifetime of the
    // HashMap, so as I don't yet know lifetimes, lets go with moving data
    // around.
    let name = String::from("John");
    let surname = String::from("Doe");

    let mut friends = HashMap::new();

    // Following the rules of ownership, the variables are going to be moved and
    // will be invalid anywhere further than here
    friends.insert(name, surname);

    let best_friend = String::from("John");
    if let Some(_) = friends.get(&best_friend) {
        println!("My best friend is in my list of friends");
    }

    let worst_enemy = String::from("Unknown");
    if let None = friends.get(&worst_enemy) {
        println!("Fortunately, my enemy isn't in my list of friends");
    }

    // HashMaps can be iterated over also, they just return a tuple of key and
    // value instead of just a value. Also, here we can see that even for can
    // acquire ownership over a variable, if we had used a reference, the
    // println below would report a problem with the value being moved
    for (team, score) in teams_scores {
        println!("{}: {}", team, score);
    }
    // println!("Teams list is {:?}", teams_scores);

    let mut scores = HashMap::new();

    // Following are the different ways to update a value on a HashMap:
    // overwriting or inserting, only inserting if not present, updating based
    // on the old value

    // Just insert, I don't care what you have
    scores.insert("Blue", 20);
    // The entry method returns an Entry independently whether the key exists or
    // not, so we can set it if it didn't with `or_insert`
    scores.entry("Yellow").or_insert(20);
    scores.entry("Blue").or_insert(10);

    // `or_insert` besides setting the value if it didn't exist, will return a
    // mutable reference (`&mut V`) to the new or existing value and by using
    // the deref operator we're able to modify that value using the previous
    // value or not
    let val = scores.entry("Blue").or_insert(100);
    *val += *val;

    // Since Blue existed but we doubled its value and Yellow was inserted with
    // 20, this should return {"Blue": 40, "Yellow": 20}
    println!("The final scores are: {:?}", scores);
}
